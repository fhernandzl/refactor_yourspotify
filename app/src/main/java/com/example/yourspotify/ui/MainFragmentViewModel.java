package com.example.yourspotify.ui;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.yourspotify.manager.SpotifyRepository;
import com.example.yourspotify.model.FavAlbum;
import com.example.yourspotify.model.FavArtist;
import com.example.yourspotify.model.FavTrack;

import java.util.Date;
import java.util.List;

import kaaes.spotify.webapi.android.models.Track;

public class MainFragmentViewModel extends ViewModel {

    private final LiveData<List<FavAlbum>> albumData;
    private final LiveData<List<FavTrack>> trackData;
    private final LiveData<List<FavArtist>> artistData;

    // Date for the weather forecast
    private SpotifyRepository spotifyRepository;

    public MainFragmentViewModel(SpotifyRepository repository) {
        spotifyRepository = repository;
        trackData=spotifyRepository.getTracks();
        albumData=spotifyRepository.getAlbums();
        artistData=spotifyRepository.getArtists();
    }

    public LiveData<List<FavAlbum>> getAlbums() {
        return albumData;
    }
    public LiveData<List<FavTrack>> getTracks() {
        return trackData;
    }
    public LiveData<List<FavArtist>> getArtists() {
        return artistData;
    }

}
