package com.example.yourspotify.ui;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.yourspotify.manager.SpotifyRepository;
import com.example.yourspotify.model.FavAlbum;
import com.example.yourspotify.model.FavArtist;
import com.example.yourspotify.model.FavTrack;

import java.util.List;

public class FavFragmentViewModel extends ViewModel {

    private final SpotifyRepository spotifyRepository;
    private final LiveData<List<FavTrack>> favTracks;
    private final LiveData<List<FavAlbum>> favAlbums;
    private final LiveData<List<FavArtist>> favArtists;

    public FavFragmentViewModel(SpotifyRepository spotifyRepository){
        this.spotifyRepository = spotifyRepository;
        this.favTracks = this.spotifyRepository.getFavTracks();
        this.favAlbums = spotifyRepository.getFavAlbums();
        this.favArtists = spotifyRepository.getFavArtists();
    }

    public LiveData<List<FavTrack>> getFavTracks(){
        return this.favTracks;
    }
    public LiveData<List<FavArtist>> getFavArtists() {
        return favArtists;
    }
    public LiveData<List<FavAlbum>> getFavAlbums() {
        return favAlbums;
    }
}
