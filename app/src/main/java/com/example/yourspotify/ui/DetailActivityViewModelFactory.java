package com.example.yourspotify.ui;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.yourspotify.manager.SpotifyRepository;

public class DetailActivityViewModelFactory extends ViewModelProvider.NewInstanceFactory{
    private final SpotifyRepository spotifyRepository;
    private final String id;

    public DetailActivityViewModelFactory(SpotifyRepository spotifyRepository, String id) {
        this.spotifyRepository = spotifyRepository;
        this.id = id;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new DetailActivityViewModel(spotifyRepository,id);
    }
}
