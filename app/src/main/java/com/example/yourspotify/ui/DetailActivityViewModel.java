package com.example.yourspotify.ui;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.yourspotify.manager.SpotifyRepository;

public class DetailActivityViewModel extends ViewModel {

    private SpotifyRepository spotifyRepository;
    private final LiveData<Boolean> foundAlbumLiveData;
    private final LiveData<Boolean> foundArtistLiveData;
    private final LiveData<Boolean> foundTrackLiveData;

    public DetailActivityViewModel(SpotifyRepository spotifyRepository, String id) {
        this.spotifyRepository = spotifyRepository;
        this.foundAlbumLiveData = spotifyRepository.getAlreadyInsertFavAlbumLiveData(id);
        this.foundArtistLiveData = spotifyRepository.getAlreadyInsertFavArtistLiveData(id);
        this.foundTrackLiveData = spotifyRepository.getAlreadyInsertFavTrackLiveData(id);
    }

    public LiveData<Boolean> getFoundAlbumLiveData() {
        return foundAlbumLiveData;
    }

    public LiveData<Boolean> getFoundArtistLiveData() {
        return foundArtistLiveData;
    }

    public LiveData<Boolean> getFoundTrackLiveData() {
        return foundTrackLiveData;
    }
}
