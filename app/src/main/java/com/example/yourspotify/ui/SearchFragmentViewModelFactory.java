package com.example.yourspotify.ui;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.yourspotify.manager.SpotifyRepository;

public class SearchFragmentViewModelFactory extends ViewModelProvider.NewInstanceFactory{

    private final SpotifyRepository spotifyRepository;
    private final String query;
    public SearchFragmentViewModelFactory(SpotifyRepository repository,String query) {
        this.spotifyRepository = repository;
        this.query = query;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new SearchFragmentViewModel(spotifyRepository,this.query);
    }
}
