package com.example.yourspotify.ui;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;

import com.example.yourspotify.manager.SpotifyRepository;

import java.util.List;

import kaaes.spotify.webapi.android.models.AlbumSimple;
import kaaes.spotify.webapi.android.models.Artist;
import kaaes.spotify.webapi.android.models.Track;

public class SearchFragmentViewModel extends ViewModel {

    private final LiveData<List<Track>> searchTracksData;
    private final LiveData<List<Artist>> searchArtistsData;
    private final LiveData<List<AlbumSimple>> searchAlbumsData;
    private SpotifyRepository spotifyRepository;

    public SearchFragmentViewModel(SpotifyRepository spotifyRepository, String query){
        this.spotifyRepository = spotifyRepository;
        this.searchAlbumsData = spotifyRepository.searchAlbums(query);
        this.searchArtistsData = spotifyRepository.searchArtists(query);
        this.searchTracksData = spotifyRepository.searchTracks(query);
    }

    public LiveData<List<Track>> getSearchTracksData() {
        return searchTracksData;
    }

    public LiveData<List<Artist>> getSearchArtistsData() {
        return searchArtistsData;
    }

    public LiveData<List<AlbumSimple>> getSearchAlbumsData() {
        return searchAlbumsData;
    }
}
