package com.example.yourspotify.ui;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.yourspotify.manager.SpotifyRepository;

public class FavFragmentViewModelFactory extends ViewModelProvider.NewInstanceFactory{

        private final SpotifyRepository spotifyRepository;

        public FavFragmentViewModelFactory(SpotifyRepository repository) {
            this.spotifyRepository = repository;
        }

        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
            //noinspection unchecked
            return (T) new FavFragmentViewModel(spotifyRepository);
        }
    }
