package com.example.yourspotify.ui;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.yourspotify.manager.SpotifyRepository;

public class MainViewModelFactory extends ViewModelProvider.NewInstanceFactory{

        private SpotifyRepository spotifyRepository;

        public MainViewModelFactory(SpotifyRepository repository) {
            this.spotifyRepository = repository;
        }

        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
            //noinspection unchecked
            return (T) new MainFragmentViewModel(spotifyRepository);
        }
    }
