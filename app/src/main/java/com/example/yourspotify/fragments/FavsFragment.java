package com.example.yourspotify.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.yourspotify.EditProfileActivity;
import com.example.yourspotify.MainActivity;
import com.example.yourspotify.R;


//TODO:: Arreglar esta clase para que funcione corrrectamente
public class FavsFragment extends Fragment{

    private FragmentManager manager;
    public static final String TRACKSFav = "TRACKS FAV";
    public static final String ARTISTSFav = "ARTISTS FAV";
    public static final String ALBUMSFav = "ALBUMS FAV";
    public static final String TAG = "Spotify FavsFragment";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        manager = getFragmentManager();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        SharedPreferences preferences = this.getActivity().getSharedPreferences("settingsApp", MainActivity.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(MainActivity.FRAGMENT,TAG);
        editor.commit();
        View view = inflater.inflate(R.layout.fragment_favourites, container, false);
        final LinearLayout favTrackLayout = view.findViewById(R.id.fav_track);
        favTrackLayout.setOnClickListener(new LinearLayout.OnClickListener(){

            @Override
            public void onClick(View v) {
                TracksFavFragment.newInstance(manager, "TracksFavFragment",TRACKSFav);
            }
        });
        final LinearLayout editProfileLayout = view.findViewById(R.id.editProfile);
        editProfileLayout.setOnClickListener(new LinearLayout.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),EditProfileActivity.class);
                startActivity(intent);
            }
        });

        final LinearLayout favArtistLayout = view.findViewById(R.id.fav_artist);
        favArtistLayout.setOnClickListener(new LinearLayout.OnClickListener(){

            @Override
            public void onClick(View v) {
                ArtistFavFragment.newInstance(manager, "ArtistFavFragment",ARTISTSFav);
            }
        });

        final LinearLayout favAlbumLayout = view.findViewById(R.id.fav_album);
        favAlbumLayout.setOnClickListener(new LinearLayout.OnClickListener(){

            @Override
            public void onClick(View v) {
                AlbumFavFragment.newInstance(manager, "AlbumFavFragment",ALBUMSFav);
            }
        });




        return view;
    }

}
