package com.example.yourspotify.fragments;


import android.app.Fragment;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.yourspotify.MainActivity;
import com.example.yourspotify.R;
import com.example.yourspotify.manager.GetApiContent;
import com.example.yourspotify.manager.SpotifyRepository;
import com.example.yourspotify.model.FavAlbum;
import com.example.yourspotify.model.FavArtist;
import com.example.yourspotify.model.FavTrack;
import com.example.yourspotify.ui.SearchFragmentViewModel;
import com.example.yourspotify.ui.SearchFragmentViewModelFactory;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import kaaes.spotify.webapi.android.models.AlbumSimple;
import kaaes.spotify.webapi.android.models.Artist;
import kaaes.spotify.webapi.android.models.Track;

public class ResultSearchAnythingFragment extends Fragment {
    public static final String QUERY = "QUERY";
    public static final String OPERATION = "OPERATION";
    private static String getOperation;
    private ListAdapter adapter;
    private RecyclerView searchListView;
    private GetApiContent.onCompleteSearchTrackListener trackListener;
    private GetApiContent.onCompleteSearchAlbumListener albumListener;
    private GetApiContent.onCompleteSearchArtistListener artistListener;
    OnSearchSelectedListener callback;
    public static final String TAGARTIST = "Spotify ResultSearchAnythingFragmentArtist";
    public static final String TAGALBUM = "Spotify ResultSearchAnythingFragmentAlbum";
    public static final String TAGTRACK = "Spotify ResultSearchAnythingFragmentTrack";

    private SpotifyRepository spotifyRepository;
    private Date currentDate;
    private SearchFragmentViewModel tViewModel;
    private static String getQuery;

    public void setOnSearchSelectedListener (OnSearchSelectedListener callback){
        this.callback = callback;
    }
    public interface OnSearchSelectedListener {
        public void showDetailSearch(Object o);
    }

    public static ResultSearchAnythingFragment newInstance (String query, String operation) {
        getQuery = query;
        getOperation = operation;
        ResultSearchAnythingFragment resultSearchTrackFragment = new ResultSearchAnythingFragment();
        return resultSearchTrackFragment;
    }



    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        LocalDate date = LocalDate.now();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            this.currentDate = dateFormat.parse(date.toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SharedPreferences preferences = this.getActivity().getSharedPreferences("settingsApp", MainActivity.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        switch(getOperation){
            case SearchFragment.TRACKS:
                editor.putString(MainActivity.FRAGMENT,TAGTRACK);

                break;
            case SearchFragment.ALBUMS:
                editor.putString(MainActivity.FRAGMENT,TAGALBUM);

                break;
            case SearchFragment.ARTISTS:
                editor.putString(MainActivity.FRAGMENT,TAGARTIST);
                break;
        }
        editor.putString(QUERY,getQuery);
        editor.commit();

        this.spotifyRepository = MainActivity.spotifyRepository;
        View view = inflater.inflate(R.layout.fragment_result_search_track, container, false);
        searchListView = view.findViewById(R.id.search_recycle_view);
        TextView header = view.findViewById(R.id.textView_searchHeader);
        TextView option = view.findViewById(R.id.textView_searchOption);

        switch (getOperation){
            case SearchFragment.TRACKS:
                header.setText("Search Tracks");
                option.setText("Tracks");
                break;
            case SearchFragment.ALBUMS:
                header.setText("Search Albums");
                option.setText("Albums");
                break;
            case SearchFragment.ARTISTS:
                header.setText("Search Artists");
                option.setText("Artists");
                break;
        }
        searchListView.setLayoutManager(new GridLayoutManager(getContext(), 1));
        adapter = new ListAdapter(getOperation);
        searchListView.setAdapter(adapter);

        SearchFragmentViewModelFactory factory = new SearchFragmentViewModelFactory(spotifyRepository,getQuery);
        tViewModel = ViewModelProviders.of((FragmentActivity) this.getActivity(),factory).get(SearchFragmentViewModel.class);
        if(getOperation == SearchFragment.ALBUMS){
            spotifyRepository.searchAlbums(getQuery);
            tViewModel.getSearchAlbumsData().observeForever(new Observer<List<AlbumSimple>>() {
                @Override
                public void onChanged(List<AlbumSimple> albumSimples) {
                    adapter.setAlbums(albumSimples);
                }
            });
        }
        if(getOperation== SearchFragment.ARTISTS){
            spotifyRepository.searchArtists(getQuery);
            tViewModel.getSearchArtistsData().observeForever(new Observer<List<Artist>>() {
                @Override
                public void onChanged(List<Artist> artists) {
                    adapter.setArtists(artists);
                }
            });
        }

        if(getOperation == SearchFragment.TRACKS){
            spotifyRepository.searchTracks(getQuery);
            tViewModel.getSearchTracksData().observeForever(new Observer<List<Track>>() {
                @Override
                public void onChanged(List<Track> tracks) {
                    adapter.setTracks(tracks);
                }
            });
        }

        return view;
    }

    private class ListHolder extends RecyclerView.ViewHolder{
        private Track track;
        private AlbumSimple album;
        private TextView name;
        private Artist artist;
        private ImageView image;
        private LinearLayout layout;
        private TextView artistText;
        public ListHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.search_name);
            layout = itemView.findViewById(R.id.search_layout);
            image = itemView.findViewById(R.id.search_image_field);
            artistText = itemView.findViewById(R.id.search_artist);
        }
        private void bind(Object o){
            if (o instanceof Track){
                track = (Track) o;
                Picasso.with(getContext())
                        .load(track.album.images.get(1).url)
                        .transform(new Transformation() {
                            @Override
                            public Bitmap transform(Bitmap source) {
                                final Bitmap copy = source.copy(source.getConfig(), true);
                                source.recycle();

                                return copy;
                            }

                            @Override
                            public String key() {
                                return track.name;
                            }
                        })
                        .into(this.image);
                name.setText(track.name);
                artistText.setText(track.artists.get(0).name);
            } else if (o instanceof AlbumSimple){
                album = (AlbumSimple) o;
                Picasso.with(getContext())
                        .load(album.images.get(1).url)
                        .transform(new Transformation() {
                            @Override
                            public Bitmap transform(Bitmap source) {
                                final Bitmap copy = source.copy(source.getConfig(), true);
                                source.recycle();

                                return copy;
                            }

                            @Override
                            public String key() {
                                return album.name;
                            }
                        })
                        .into(this.image);
                name.setText(album.name);
                artistText.setText("");


            } else if (o instanceof Artist){
                artist = (Artist) o;
                Picasso.with(getContext())
                        .load(artist.images.get(1).url)
                        .transform(new Transformation() {
                            @Override
                            public Bitmap transform(Bitmap source) {
                                final Bitmap copy = source.copy(source.getConfig(), true);
                                source.recycle();

                                return copy;
                            }

                            @Override
                            public String key() {
                                return artist.name;
                            }
                        })
                        .into(this.image);
                name.setText(artist.name);
                artistText.setText("");


            }

        }
    }

    private class ListAdapter extends RecyclerView.Adapter<ListHolder>{
        private List <Track> tracks;
        private List <Artist> artists;
        private String operation;
        private List <AlbumSimple> albums;
        private ListAdapter(String operation){
            switch (operation){
                case SearchFragment.TRACKS:
                    tracks = new ArrayList<Track>();
                    break;
                case SearchFragment.ALBUMS:
                    albums = new ArrayList<AlbumSimple>();
                    break;
                case SearchFragment.ARTISTS:
                    artists = new ArrayList<Artist>();
                    break;
            }
            this.operation = operation;
        }
        public void setTracks(List<Track> tracks) {
            if (tracks != null){
                this.tracks = tracks;
                this.operation = SearchFragment.TRACKS;
                this.notifyDataSetChanged();
            }
        }

        public void setArtists(List<Artist> artists) {
            if (artists != null){
                if (artists.size() >= 1){
                    this.artists.clear();
                    this.artists.add(artists.get(0));
                    this.operation = SearchFragment.ARTISTS;
                    this.notifyDataSetChanged();
                } else
                    this.notifyDataSetChanged();
            }

        }
        public void setAlbums (List<AlbumSimple> albums){
            if (albums != null){
                this.albums = albums;
                this.operation = SearchFragment.ALBUMS;
                this.notifyDataSetChanged();
            }
        }


        @NonNull
        @Override
        public ListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater.inflate(R.layout.anything_search, parent, false);
            return new ListHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ListHolder holder, final int position) {
            switch (operation){
                case SearchFragment.TRACKS:
                    holder.bind(tracks.get(position));
                    holder.layout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            FavTrack track = new FavTrack(tracks.get(position).id,tracks.get(position).name,
                                    tracks.get(position).album.images.get(1).url,tracks.get(position).external_urls.get("spotify"),
                                    currentDate,tracks.get(position).artists.get(0).name,
                                    spotifyRepository.getAlreadyInsertFavTrack(tracks.get(position).id),
                                    tracks.get(position).album.name, tracks.get(position).album.external_urls.get("spotify"),
                                    tracks.get(position).artists.get(0).external_urls.get("spotify"));

                            callback.showDetailSearch(track);
                        }
                    });
                    break;
                case SearchFragment.ARTISTS:
                    holder.bind(artists.get(position));
                    holder.layout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            FavArtist artist = new FavArtist(artists.get(position).id,artists.get(position).name,
                                    artists.get(position).images.get(1).url,
                                    artists.get(position).external_urls.get("spotify"),
                                    spotifyRepository.getAlreadyInsertFavArtist(artists.get(position).id),currentDate);
                            callback.showDetailSearch(artist);
                        }
                    });
                    break;
                case SearchFragment.ALBUMS:
                    holder.bind(albums.get(position));
                    holder.layout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            FavAlbum album = new FavAlbum(albums.get(position).id,albums.get(position).name,albums.get(position).images.get(1).url
                                    ,albums.get(position).external_urls.get("spotify"),currentDate,
                                    spotifyRepository.getAlreadyInsertFavAlbum(albums.get(position).id));
                            callback.showDetailSearch(album);
                        }
                    });
                    break;
            }
        }



        @Override
        public int getItemCount() {
            int size = 0;
            switch (operation){
                case SearchFragment.TRACKS:
                    size = tracks.size();
                    break;
                case SearchFragment.ARTISTS:
                    size = artists.size();
                    break;
                case SearchFragment.ALBUMS:
                    size = albums.size();
                    break;
            }
            return size;
        }
    }


}
