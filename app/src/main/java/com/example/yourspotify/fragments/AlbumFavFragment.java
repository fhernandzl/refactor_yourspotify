package com.example.yourspotify.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.yourspotify.MainActivity;
import com.example.yourspotify.R;
import com.example.yourspotify.manager.SpotifyRepository;
import com.example.yourspotify.model.FavAlbum;
import com.example.yourspotify.ui.FavFragmentViewModel;
import com.example.yourspotify.ui.FavFragmentViewModelFactory;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.ArrayList;
import java.util.List;

public class AlbumFavFragment extends Fragment {

    public static final String TAG = "Spotify AlbumFavsFragment";
    private RecyclerView mFavAlbumRecyclerView;
    private VerticalAlbumAdapter albumAdapter;
    private SpotifyRepository spotifyRepository;
    private FavFragmentViewModel tViewModel;

    OnTrackFavSelectedListener callback;
    public void setOnHomeSelectedListener(OnTrackFavSelectedListener callback) {
        this.callback = callback;
    }

    public interface OnTrackFavSelectedListener {
        public void showDetailFavAlbum(FavAlbum t);
    }

    public void setSpotifyRepository(SpotifyRepository spotifyRepository) {
        this.spotifyRepository = spotifyRepository;
    }

    public static AlbumFavFragment newInstance(FragmentManager fragmentManager, String tag, String operation) {
        AlbumFavFragment fragment = (AlbumFavFragment) fragmentManager.findFragmentByTag(tag);
            fragment = new AlbumFavFragment();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        if(operation == "reboot"){
            ft.replace(R.id.fragment,fragment,tag).commit();

        } else
            ft.replace(R.id.fragment,fragment,tag).addToBackStack(null).commit();

        ft.addToBackStack(null);
            fragment.setSpotifyRepository(MainActivity.spotifyRepository);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        SharedPreferences preferences = this.getActivity().getSharedPreferences("settingsApp", MainActivity.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(MainActivity.FRAGMENT,TAG);
        editor.commit();

        View view = inflater.inflate(R.layout.fragment_albumfav, container, false);

        mFavAlbumRecyclerView = view.findViewById(R.id.top_albums_RecyclerView);
        mFavAlbumRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        albumAdapter = new VerticalAlbumAdapter();
        mFavAlbumRecyclerView.setAdapter(albumAdapter);

        FavFragmentViewModelFactory factory = new FavFragmentViewModelFactory(this.spotifyRepository);
        tViewModel = ViewModelProviders.of((FragmentActivity) this.getActivity(),factory).get(FavFragmentViewModel.class);
        tViewModel.getFavAlbums().observeForever(new Observer<List<FavAlbum>>() {
            @Override
            public void onChanged(List<FavAlbum> favAlbums) {
                Log.d("Observer","Changes detected, size: "+favAlbums.size());
                albumAdapter.setAlbums(favAlbums);
            }
        });
        return view;
    }

    private class VerticalAlbumHolder extends RecyclerView.ViewHolder{

        private ImageView album_image_field;
        private TextView album_name_view;

        private VerticalAlbumHolder(View itemView) {
            super(itemView);

            album_image_field = (ImageView) itemView.findViewById(R.id.album_image_field);
            album_name_view = (TextView) itemView.findViewById(R.id.album_name);
        }

        private void bindAlbum(final FavAlbum album){

            this.album_name_view.setText(album.getTitle());

            Picasso.with(getContext())
                    .load(album.getImg_url())
                    .transform(new Transformation() {
                        @Override
                        public Bitmap transform(Bitmap source) {
                            final Bitmap copy = source.copy(source.getConfig(), true);
                            source.recycle();

                            return copy;
                        }

                        @Override
                        public String key() {
                            return album.getTitle();
                        }
                    })
                    .into(this.album_image_field);



        }

    }

    private class VerticalAlbumAdapter extends RecyclerView.Adapter<VerticalAlbumHolder>{

        private List<FavAlbum> albums;

        private VerticalAlbumAdapter(){
            albums = new ArrayList<FavAlbum>();
        }

        public void setAlbums(List<FavAlbum> t) {
            this.albums = t;
            this.notifyDataSetChanged();
        }


        @Override
        public VerticalAlbumHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            LayoutInflater inflater = LayoutInflater.from(getContext());

            View view = inflater.inflate(R.layout.album_view, parent, false);

            return new VerticalAlbumHolder(view);
        }

        @Override
        public void onBindViewHolder(VerticalAlbumHolder holder, final int position) {
            holder.bindAlbum(albums.get(position));
            holder.album_image_field.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    callback.showDetailFavAlbum(albums.get(position));
                }
            });
        }

        @Override
        public int getItemCount() {
            return albums.size();
        }

    }

    }



