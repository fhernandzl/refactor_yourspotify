package com.example.yourspotify.fragments;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import android.app.Fragment;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.yourspotify.MainActivity;
import com.example.yourspotify.R;
import com.example.yourspotify.manager.GetApiContent;
import com.example.yourspotify.manager.SpotifyRepository;
import com.example.yourspotify.model.FavTrack;
import com.example.yourspotify.ui.MainFragmentViewModel;
import com.example.yourspotify.ui.MainViewModelFactory;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


//TODO:: Arreglar esta clase para que funcione corrrectamente
public class HomeFragment extends Fragment{

    public static final String TAG = "Spotify HomeFragment";
    private RecyclerView mTopTrackRecyclerView;
    private VerticalTrackAdapter trackAdapter;
    private GetApiContent.onCompleteTopTrackListener trackListener;
    private SpotifyRepository spotifyRepository;
    private MainFragmentViewModel mFragmentViewModel;

    OnHomeSelectedListener callback;
    private Date currentDate;

    public void setOnHomeSelectedListener(OnHomeSelectedListener callback) {
        this.callback = callback;
    }

    public interface OnHomeSelectedListener {
        public void showDetailTopTrack (FavTrack t);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        SharedPreferences preferences = this.getActivity().getSharedPreferences("settingsApp", MainActivity.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(MainActivity.FRAGMENT,TAG);
        editor.commit();
        LocalDate date = LocalDate.now();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            this.currentDate = dateFormat.parse(date.toString());
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        spotifyRepository = MainActivity.spotifyRepository;

        View view = inflater.inflate(R.layout.fragment_home,container,false);

        mTopTrackRecyclerView = view.findViewById(R.id.top_tracks_RecyclerView);
        mTopTrackRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        trackAdapter = new VerticalTrackAdapter();
        mTopTrackRecyclerView.setAdapter(trackAdapter);

        MainViewModelFactory factory = new MainViewModelFactory(spotifyRepository);
        mFragmentViewModel = ViewModelProviders.of((FragmentActivity) this.getActivity(),factory).get(MainFragmentViewModel.class);

        mFragmentViewModel.getTracks().observeForever(new Observer<List<FavTrack>>() {
            @Override
            public void onChanged(List<FavTrack> favTracks) {
                Log.d("Observer","Changes detected, size: "+favTracks.size());
                trackAdapter.setTracks(favTracks);
            }
        });
        //updateUI();

        return view;
    }


    private class VerticalTrackHolder extends RecyclerView.ViewHolder{

        private ImageView album_image_field;
        private TextView album_name_view;

        private VerticalTrackHolder(View itemView) {
            super(itemView);

            album_image_field = (ImageView) itemView.findViewById(R.id.track_image_field);
            album_name_view = (TextView) itemView.findViewById(R.id.track_name);
        }

        private void bindTrack(final FavTrack track){

            this.album_name_view.setText(track.getName());

            Picasso.with(getContext())
                    .load(track.getImg_url())
                    .transform(new Transformation() {
                        @Override
                        public Bitmap transform(Bitmap source) {
                            final Bitmap copy = source.copy(source.getConfig(), true);
                            source.recycle();

                            return copy;
                        }

                        @Override
                        public String key() {
                            return track.getName();
                        }
                    })
                    .into(this.album_image_field);



        }

    }

    private class VerticalTrackAdapter extends RecyclerView.Adapter<VerticalTrackHolder>{

        private List<FavTrack> tracks;

        private VerticalTrackAdapter(){
            tracks = new ArrayList<FavTrack>();
        }

        public void setTracks(List<FavTrack> t) {
            this.tracks = t;
            this.notifyDataSetChanged();
        }

        @Override
        public VerticalTrackHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            LayoutInflater inflater = LayoutInflater.from(getContext());

            View view = inflater.inflate(R.layout.track_view, parent, false);

            return new VerticalTrackHolder(view);
        }

        @Override
        public void onBindViewHolder(VerticalTrackHolder holder,final int position) {
            holder.bindTrack(tracks.get(position));
            holder.album_image_field.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    callback.showDetailTopTrack(tracks.get(position));
                }
            });
        }

        @Override
        public int getItemCount() {
            return tracks.size();
        }

    }
}
