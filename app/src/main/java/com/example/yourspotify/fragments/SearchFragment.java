package com.example.yourspotify.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.yourspotify.MainActivity;
import com.example.yourspotify.R;

public class SearchFragment extends Fragment {
    private FragmentManager manager;
    public static final String TAG = "Spotify SearchFragment";
    public static final String SearchFragments = "Search Fragments";
    public static final String TRACKS = "SEARCH TRACKS";
    public static final String ARTISTS = "SEARCH ARTISTS";
    public static final String ALBUMS = "SEARCH ALBUMS";

    public static SearchFragment getFragmentInstance(FragmentManager fm, String tag){
        SearchFragment fragment = (SearchFragment)fm.findFragmentByTag(tag);

        if(fragment == null){
            fragment = new SearchFragment();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.fragment, fragment, tag).commitAllowingStateLoss();
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        manager = getFragmentManager();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        SharedPreferences preferences = this.getActivity().getSharedPreferences("settingsApp", MainActivity.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(MainActivity.FRAGMENT,TAG);
        editor.commit();


        View view = inflater.inflate(R.layout.fragment_search, container, false);
        final LinearLayout searchTrackLayout= view.findViewById(R.id.layout_search_track);
        searchTrackLayout.setOnClickListener(new LinearLayout.OnClickListener(){

            @Override
            public void onClick(View v) {
                manager.beginTransaction().replace(R.id.fragment, new SearchAnythingFragment(TRACKS)).addToBackStack(null)
                        .commit();

            }
        });
        final LinearLayout searchAlbumLayout=view.findViewById(R.id.layout_search_album);
        searchAlbumLayout.setOnClickListener(new LinearLayout.OnClickListener() {
            @Override
            public void onClick(View v) {
                manager.beginTransaction().replace(R.id.fragment, new SearchAnythingFragment(ALBUMS)).addToBackStack(null)
                        .commit();

            }
        });
        final LinearLayout searchArtistLayout=view.findViewById(R.id.layout_search_artist);
        searchArtistLayout.setOnClickListener(new LinearLayout.OnClickListener() {
            @Override
            public void onClick(View v) {
                manager.beginTransaction().replace(R.id.fragment, new SearchAnythingFragment(ARTISTS)).addToBackStack(null)
                        .commit();

            }
        });
        return view;

    }

}
