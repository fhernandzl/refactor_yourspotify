package com.example.yourspotify.fragments;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import android.app.Fragment;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.yourspotify.MainActivity;
import com.example.yourspotify.R;
import com.example.yourspotify.manager.SpotifyRepository;
import com.example.yourspotify.model.FavAlbum;
import com.example.yourspotify.ui.MainFragmentViewModel;
import com.example.yourspotify.ui.MainViewModelFactory;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TopAlbumsFragment extends Fragment {

    public static final String TAG = "Spotify TopAlbumsFragment";
    private RecyclerView mTopTrackRecyclerView;
    private VerticalAlbumAdapter albumAdapter;
    private SpotifyRepository spotifyRepository;
    private Date currentDate;
    private MainFragmentViewModel tViewModel;

    AlbumSelectedListener callback;
    public void setOnAlbumSelectedListener(AlbumSelectedListener callback) {
        this.callback = callback;
    }

    public interface AlbumSelectedListener {
        public void showDetailTopAlbum (FavAlbum t);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        SharedPreferences preferences = this.getActivity().getSharedPreferences("settingsApp", MainActivity.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(MainActivity.FRAGMENT,TAG);
        editor.commit();

        // Inflate the layout for this fragment
        LocalDate date = LocalDate.now();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            this.currentDate = dateFormat.parse(date.toString());
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        spotifyRepository = MainActivity.spotifyRepository;

        View view = inflater.inflate(R.layout.fragment_albums,container,false);

        mTopTrackRecyclerView = view.findViewById(R.id.top_albums_RecyclerView);
        mTopTrackRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        albumAdapter = new VerticalAlbumAdapter();
        mTopTrackRecyclerView.setAdapter(albumAdapter);


        MainViewModelFactory factory = new MainViewModelFactory(this.spotifyRepository);
        tViewModel = ViewModelProviders.of((FragmentActivity) this.getActivity(),factory).get(MainFragmentViewModel.class);
        tViewModel.getAlbums().observeForever( new Observer<List<FavAlbum>>() {
            @Override
            public void onChanged(List<FavAlbum> favAlbums) {
                albumAdapter.setAlbums(favAlbums);
            }
        });
        return view;
    }


    private class VerticalAlbumHolder extends RecyclerView.ViewHolder{

        private ImageView album_image_field;
        private TextView album_name_view;

        private VerticalAlbumHolder(View itemView) {
            super(itemView);

            album_image_field = (ImageView) itemView.findViewById(R.id.track_image_field);
            album_name_view = (TextView) itemView.findViewById(R.id.track_name);
        }

        private void bindAlbum(final FavAlbum album){

            this.album_name_view.setText(album.getTitle());

            Picasso.with(getContext())
                    .load(album.getImg_url())
                    .transform(new Transformation() {
                        @Override
                        public Bitmap transform(Bitmap source) {
                            final Bitmap copy = source.copy(source.getConfig(), true);
                            source.recycle();

                            return copy;
                        }

                        @Override
                        public String key() {
                            return album.getTitle();
                        }
                    })
                    .into(this.album_image_field);



        }

    }

    private class VerticalAlbumAdapter extends RecyclerView.Adapter<VerticalAlbumHolder>{

        private List<FavAlbum> albums;

        private VerticalAlbumAdapter(){
            albums = new ArrayList<FavAlbum>();
        }

        public void setAlbums(List<FavAlbum> t) {
            this.albums = t;
            this.notifyDataSetChanged();
        }

        @Override
        public VerticalAlbumHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            LayoutInflater inflater = LayoutInflater.from(getContext());

            View view = inflater.inflate(R.layout.track_view, parent, false);

            return new VerticalAlbumHolder(view);
        }

        @Override
        public void onBindViewHolder(VerticalAlbumHolder holder, final int position) {
            holder.bindAlbum(albums.get(position));
            holder.album_image_field.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    callback.showDetailTopAlbum(albums.get(position));
                }
            });
        }

        @Override
        public int getItemCount() {
            return albums.size();
        }

    }
}
