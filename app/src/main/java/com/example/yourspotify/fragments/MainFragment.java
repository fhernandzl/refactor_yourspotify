package com.example.yourspotify.fragments;


import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.yourspotify.EditProfileActivity;
import com.example.yourspotify.MainActivity;
import com.example.yourspotify.R;
import com.example.yourspotify.manager.SpotifyRepository;

//TODO:: Revisar esta clase para que funcione corrrectamente

public class MainFragment extends Fragment {

    private static final String TAG = "Spotify MainFragment";

    private FragmentManager manager;

    private TextView trackText;
    private TextView artistText;
    private TextView searchText;
    private TextView albumText;
    private TextView libraryText;
    private TextView settingsText;
    public SpotifyRepository spotifyRepository;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        manager = getFragmentManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        spotifyRepository = MainActivity.spotifyRepository;
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        Button trackLayout = view.findViewById(R.id.nav_track);
        Button artistLayout = view.findViewById(R.id.nav_artist);
        Button searchLayout = view.findViewById(R.id.nav_search);
        Button albumLayout = view.findViewById(R.id.nav_album);
        Button libraryLayout = view.findViewById(R.id.nav_library);
        Button settingsLayout = view.findViewById(R.id.settings);

        trackText = view.findViewById(R.id.nav_track_text);
        artistText = view.findViewById(R.id.nav_artist_text);
        searchText = view.findViewById(R.id.nav_search_text);
        albumText = view.findViewById(R.id.nav_album_text);
        libraryText = view.findViewById(R.id.nav_library_text);
        settingsText = view.findViewById(R.id.settings_text);

        trackLayout.setOnClickListener(mListener);
        artistLayout.setOnClickListener(mListener);
        searchLayout.setOnClickListener(mListener);
        albumLayout.setOnClickListener(mListener);
        libraryLayout.setOnClickListener(mListener);
        settingsLayout.setOnClickListener(mListener);
        SharedPreferences preferences = this.getActivity().getSharedPreferences("settingsApp", MainActivity.MODE_PRIVATE);
        String fragmentActual = preferences.getString(MainActivity.FRAGMENT,"");
                FragmentManager manager = getFragmentManager();
                if (fragmentActual != ""){
                    String query;
                    switch (fragmentActual){
                        case HomeFragment.TAG:
                            manager.beginTransaction().replace(R.id.fragment, new HomeFragment()).commit();
                            break;
                        case TopAlbumsFragment.TAG:
                            manager.beginTransaction().replace(R.id.fragment, new TopAlbumsFragment()).commit();
                            break;
                        case TopArtistsFragment.TAG:
                            manager.beginTransaction().replace(R.id.fragment, new TopArtistsFragment()).commit();
                            break;
                        case SearchFragment.TAG:
                            manager.beginTransaction().replace(R.id.fragment, new SearchFragment()).commit();
                            break;
                        case ResultSearchAnythingFragment.TAGALBUM:
                             query = preferences.getString(ResultSearchAnythingFragment.QUERY,"");
                            manager.beginTransaction().replace(R.id.fragment, ResultSearchAnythingFragment.newInstance(query,SearchFragment.ALBUMS)).commit();
                            break;
                        case ResultSearchAnythingFragment.TAGARTIST:
                             query = preferences.getString(ResultSearchAnythingFragment.QUERY,"");
                            manager.beginTransaction().replace(R.id.fragment, ResultSearchAnythingFragment.newInstance(query,SearchFragment.ARTISTS)).commit();
                            break;
                        case ResultSearchAnythingFragment.TAGTRACK:
                             query = preferences.getString(ResultSearchAnythingFragment.QUERY,"");
                            manager.beginTransaction().replace(R.id.fragment, ResultSearchAnythingFragment.newInstance(query,SearchFragment.TRACKS)).commit();
                            break;
                        case FavsFragment
                                .TAG:
                            manager.beginTransaction().replace(R.id.fragment, new FavsFragment()).commit();
                            break;
                        case ArtistFavFragment.TAG:
                            ArtistFavFragment.newInstance(manager, "ArtistFavFragment","reboot");
                            break;
                        case AlbumFavFragment.TAG:
                            AlbumFavFragment.newInstance(manager, "AlbumFavFragment","reboot");
                            break;
                        case TracksFavFragment.TAG:
                            TracksFavFragment.newInstance(manager, "TracksFavFragment","reboot");
                            break;
                        default:
                            manager.beginTransaction().replace(R.id.fragment, new HomeFragment()).commit();

                    }

                } else
                    manager.beginTransaction().replace(R.id.fragment, new HomeFragment()).commit();


        return view;
    }

    Drawable track;
    Drawable artist;
    Drawable search;
    Drawable album;
    Drawable library;
    Drawable settings;
    int focusMode;
    int defocusMode;

    int prev_clicked_id = -1;
    View prev_view;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);


        track = getResources().getDrawable(R.drawable.ic_music_video_black_24dp, null);
        artist = getResources().getDrawable(R.drawable.artist, null);
        search = getResources().getDrawable(R.drawable.ic_search_black_24dp, null);
        album = getResources().getDrawable(R.drawable.ic_iconfinder_music_album_3671818, null);
        library = getResources().getDrawable(R.drawable.ic_library_music_black_24dp, null);
        settings = getResources().getDrawable(R.drawable.ic_more_vert_black_24dp,null);

        focusMode = getResources().getColor(R.color.colorWhite, null);;
        defocusMode = getResources().getColor(R.color.colorNavIcon, null);

    }

    View.OnClickListener mListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId())

            {
                case R.id.nav_track:
                    if(view.isActivated()) break;

                    manager.beginTransaction().replace(R.id.fragment, new HomeFragment()).addToBackStack(null).commit();
                    if(view.isActivated()) break;


                    setFocus(R.id.nav_track, view);
                    setDeFocus(prev_clicked_id, prev_view);

                    prev_clicked_id = R.id.nav_track;
                    prev_view = view;
                    break;
                case R.id.nav_artist:
                    if(view.isActivated()) break;

                    manager.beginTransaction().replace(R.id.fragment, new TopArtistsFragment()).addToBackStack(null).commit();

                    setFocus(R.id.nav_artist, view);
                    setDeFocus(prev_clicked_id, prev_view);

                    prev_clicked_id = R.id.nav_artist;
                    prev_view = view;
                    break;
                case R.id.nav_search:

                    if(view.isActivated()) break;
                     manager.beginTransaction().replace(R.id.fragment, new SearchFragment()).addToBackStack(null).commit();
                    setFocus(R.id.nav_search, view);
                    setDeFocus(prev_clicked_id, prev_view);

                    prev_clicked_id = R.id.nav_search;
                    prev_view = view;
                    break;
                case R.id.nav_album:

                    if(view.isActivated()) break;

                    manager.beginTransaction().replace(R.id.fragment, new TopAlbumsFragment()).commit();

                    setFocus(R.id.nav_album, view);
                    setDeFocus(prev_clicked_id, prev_view);

                    prev_clicked_id = R.id.nav_album;
                    prev_view = view;
                    break;
                case R.id.nav_library:

                    if(view.isActivated()) break;

                    manager.beginTransaction().replace(R.id.fragment, new FavsFragment()).addToBackStack(null).commit();

                    setFocus(R.id.nav_library, view);
                    setDeFocus(prev_clicked_id, prev_view);

                    prev_clicked_id = R.id.nav_library;
                    prev_view = view;
                    break;

                case R.id.settings:
                    Intent intent = new Intent(getActivity(),EditProfileActivity.class);
                    startActivity(intent);
                    break;
            }
        }
    };

    private void setFocus(int res_id, View view){
        switch (res_id)
        {
            case R.id.nav_track:
                track.setTint(focusMode);
                view.setBackground(track);
                trackText.setTextColor(focusMode);
                trackText.setTypeface(Typeface.DEFAULT_BOLD);
                view.setActivated(true);
                break;
            case R.id.nav_artist:
                artist.setTint(focusMode);
                view.setBackground(artist);
                artistText.setTextColor(focusMode);
                artistText.setTypeface(Typeface.DEFAULT_BOLD);
                view.setActivated(true);
                break;
            case R.id.nav_search:
                search.setTint(focusMode);
                view.setBackground(search);
                searchText.setTextColor(focusMode);
                searchText.setTypeface(Typeface.DEFAULT_BOLD);
                view.setActivated(true);
                break;
            case R.id.nav_album:
                album.setTint(focusMode);
                view.setBackground(album);
                albumText.setTextColor(focusMode);
                albumText.setTypeface(Typeface.DEFAULT_BOLD);
                view.setActivated(true);
                break;
            case R.id.nav_library:
                library.setTint(focusMode);
                view.setBackground(library);
                libraryText.setTextColor(focusMode);
                libraryText.setTypeface(Typeface.DEFAULT_BOLD);
                view.setActivated(true);
                break;
        }
    }
    private void setDeFocus(int res_id, View view){
        switch (res_id)
        {
            case R.id.nav_track:
                track.setTint(defocusMode);
                view.setBackground(track);
                trackText.setTextColor(defocusMode);
                trackText.setTypeface(Typeface.DEFAULT);
                view.setActivated(false);
                break;
            case R.id.nav_artist:
                artist.setTint(defocusMode);
                view.setBackground(artist);
                artistText.setTextColor(defocusMode);
                artistText.setTypeface(Typeface.DEFAULT);
                view.setActivated(false);
                break;
            case R.id.nav_search:
                search.setTint(defocusMode);
                view.setBackground(search);
                searchText.setTextColor(defocusMode);
                searchText.setTypeface(Typeface.DEFAULT);
                view.setActivated(false);
                break;
            case R.id.nav_album:
                album.setTint(defocusMode);
                view.setBackground(album);
                albumText.setTextColor(defocusMode);
                albumText.setTypeface(Typeface.DEFAULT);
                view.setActivated(false);
                break;
            case R.id.nav_library:
                library.setTint(defocusMode);
                view.setBackground(library);
                libraryText.setTextColor(defocusMode);
                libraryText.setTypeface(Typeface.DEFAULT);
                view.setActivated(false);
                break;
            case -1:
                break;
            default:
                break;
        }
    }
}
