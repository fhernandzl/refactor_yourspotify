package com.example.yourspotify.fragments;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.yourspotify.MainActivity;
import com.example.yourspotify.R;
import com.example.yourspotify.manager.SpotifyRepository;
import com.example.yourspotify.model.FavArtist;
import com.example.yourspotify.ui.MainFragmentViewModel;
import com.example.yourspotify.ui.MainViewModelFactory;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class TopArtistsFragment extends Fragment{

    public static final String TAG = "Spotify TopArtistsFragment";
    private RecyclerView mTopTrackRecyclerView;
    private VerticalArtistAdapter artistAdapter;
    private SpotifyRepository spotifyRepository;
    ArtistSelectedListener callback;
    private MainFragmentViewModel tViewModel;
    private Date currentDate;


    public void setOnArtistSelectedListener(ArtistSelectedListener callback) {
        this.callback = callback;
    }

    public interface ArtistSelectedListener {
        public void showDetailTopArtist (FavArtist t);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        SharedPreferences preferences = this.getActivity().getSharedPreferences("settingsApp", MainActivity.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(MainActivity.FRAGMENT,TAG);
        editor.commit();

        LocalDate date = LocalDate.now();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            this.currentDate = dateFormat.parse(date.toString());
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        spotifyRepository = MainActivity.spotifyRepository;

        View view = inflater.inflate(R.layout.fragment_artists,container,false);

        mTopTrackRecyclerView = view.findViewById(R.id.top_artists_RecyclerView);
        mTopTrackRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        artistAdapter = new VerticalArtistAdapter();
        mTopTrackRecyclerView.setAdapter(artistAdapter);
        MainViewModelFactory factory = new MainViewModelFactory(this.spotifyRepository);
        tViewModel = ViewModelProviders.of((FragmentActivity) this.getActivity(),factory).get(MainFragmentViewModel.class);
        tViewModel.getArtists().observeForever(new Observer<List<FavArtist>>() {
            @Override
            public void onChanged(List<FavArtist> favArtists) {
                artistAdapter.setArtists(favArtists);
            }
        });
        return view;
    }


    private class VerticalArtistHolder extends RecyclerView.ViewHolder{

        private ImageView artist_image_field;
        private TextView artist_name;

        private VerticalArtistHolder(View itemView) {
            super(itemView);

            artist_image_field = (ImageView) itemView.findViewById(R.id.artist_image_field);
            artist_name = (TextView) itemView.findViewById(R.id.artist_name);
        }

        private void bindArtist(final FavArtist artist){

            this.artist_name.setText(artist.getName());

            Picasso.with(getContext())
                    .load(artist.getImg_url())
                    .transform(new Transformation() {
                        @Override
                        public Bitmap transform(Bitmap source) {
                            final Bitmap copy = source.copy(source.getConfig(), true);
                            source.recycle();

                            return copy;
                        }

                        @Override
                        public String key() {
                            return artist.getName();
                        }
                    })
                    .into(this.artist_image_field);



        }

    }

    private class VerticalArtistAdapter extends RecyclerView.Adapter<VerticalArtistHolder>{

        private List<FavArtist> artists;

        private VerticalArtistAdapter(){
            artists = new ArrayList<FavArtist>();
        }

        public void setArtists(List<FavArtist> t) {
            this.artists = t;
            this.notifyDataSetChanged();
        }

        @Override
        public VerticalArtistHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            LayoutInflater inflater = LayoutInflater.from(getContext());

            View view = inflater.inflate(R.layout.artist_view, parent, false);

            return new VerticalArtistHolder(view);
        }

        @Override
        public void onBindViewHolder(VerticalArtistHolder holder, final int position) {
            holder.bindArtist(artists.get(position));
            holder.artist_image_field.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    callback.showDetailTopArtist(artists.get(position));
                }
            });
        }

        @Override
        public int getItemCount() {
            return artists.size();
        }

    }
}
