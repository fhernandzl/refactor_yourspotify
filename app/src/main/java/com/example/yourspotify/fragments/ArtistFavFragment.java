package com.example.yourspotify.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.yourspotify.MainActivity;
import com.example.yourspotify.R;
import com.example.yourspotify.manager.SpotifyRepository;
import com.example.yourspotify.model.FavArtist;
import com.example.yourspotify.ui.FavFragmentViewModel;
import com.example.yourspotify.ui.FavFragmentViewModelFactory;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.ArrayList;
import java.util.List;

public class ArtistFavFragment extends Fragment {

    public static final String TAG = "Spotify ArtistFavsFragment";
    private RecyclerView mFavArtistRecyclerView;
    private VerticalArtistAdapter artistAdapter;
    private FavFragmentViewModel tViewModel;
    private SpotifyRepository spotifyRepository;

    OnTrackFavSelectedListener callback;
    public void setOnHomeSelectedListener(OnTrackFavSelectedListener callback) {
        this.callback = callback;
    }

    public interface OnTrackFavSelectedListener {
        public void showDetailFavArtist (FavArtist t);
    }

    public void setSpotifyRepository(SpotifyRepository spotifyRepository) {
        this.spotifyRepository = spotifyRepository;
    }

    public static ArtistFavFragment newInstance(FragmentManager fragmentManager, String tag, String operation) {
        ArtistFavFragment fragment = (ArtistFavFragment) fragmentManager.findFragmentByTag(tag);
            fragment = new ArtistFavFragment();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        if(operation == "reboot"){
            ft.replace(R.id.fragment,fragment,tag).commit();

        } else
            ft.replace(R.id.fragment,fragment,tag).addToBackStack(null).commit();

        fragment.setSpotifyRepository(MainActivity.spotifyRepository);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        SharedPreferences preferences = this.getActivity().getSharedPreferences("settingsApp", MainActivity.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(MainActivity.FRAGMENT,TAG);
        editor.commit();

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_artistfav, container, false);

        mFavArtistRecyclerView = view.findViewById(R.id.top_artists_RecyclerView);
        mFavArtistRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        artistAdapter = new VerticalArtistAdapter();
        mFavArtistRecyclerView.setAdapter(artistAdapter);
        FavFragmentViewModelFactory factory = new FavFragmentViewModelFactory(this.spotifyRepository);
        tViewModel = ViewModelProviders.of((FragmentActivity) this.getActivity(),factory).get(FavFragmentViewModel.class);
        tViewModel.getFavArtists().observeForever(new Observer<List<FavArtist>>() {
            @Override
            public void onChanged(List<FavArtist> favArtists) {
                Log.d("Observer","Changes detected, size: "+favArtists.size());
                artistAdapter.setArtists(favArtists);
            }
        });
        return view;
    }



    private class VerticalArtistHolder extends RecyclerView.ViewHolder{

        private ImageView artist_image_field;
        private TextView artist_name;

        private VerticalArtistHolder(View itemView) {
            super(itemView);

            artist_image_field = (ImageView) itemView.findViewById(R.id.artist_image_field);
            artist_name = (TextView) itemView.findViewById(R.id.artist_name);
        }

        private void bindArtist(final FavArtist artist){


            this.artist_name.setText(artist.getName());

            Picasso.with(getContext())
                    .load(artist.getImg_url())
                    .transform(new Transformation() {
                        @Override
                        public Bitmap transform(Bitmap source) {
                            final Bitmap copy = source.copy(source.getConfig(), true);
                            source.recycle();

                            return copy;
                        }

                        @Override
                        public String key() {
                            return artist.getName();
                        }
                    })
                    .into(this.artist_image_field);



        }

    }

    private class VerticalArtistAdapter extends RecyclerView.Adapter<VerticalArtistHolder>{

        private List<FavArtist> artists;

        private VerticalArtistAdapter(){
            artists = new ArrayList<FavArtist>();
        }

        public void setArtists(List<FavArtist> t) {
            this.artists = t;
            this.notifyDataSetChanged();
        }


        @Override
        public VerticalArtistHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            LayoutInflater inflater = LayoutInflater.from(getContext());

            View view = inflater.inflate(R.layout.artist_view, parent, false);

            return new VerticalArtistHolder(view);
        }

        @Override
        public void onBindViewHolder(VerticalArtistHolder holder, final int position) {
            holder.bindArtist(artists.get(position));
            holder.artist_image_field.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    callback.showDetailFavArtist(artists.get(position));
                }
            });
        }

        @Override
        public int getItemCount() {
            return artists.size();
        }

    }

}

