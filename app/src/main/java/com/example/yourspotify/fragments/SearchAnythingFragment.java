package com.example.yourspotify.fragments;


import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.view.ViewCompat;
import androidx.core.view.ViewPropertyAnimatorListener;

import com.example.yourspotify.R;

import iammert.com.view.scalinglib.ScalingLayout;
import iammert.com.view.scalinglib.ScalingLayoutListener;
import iammert.com.view.scalinglib.State;


@SuppressLint("ValidFragment")
public class SearchAnythingFragment extends Fragment {
    private static final String TAG = "Spotify SearchAnythingFragment";
    FragmentManager fragmentManager;
    private TextView textViewSearch;
    private EditText editTextSearch;
    private ScalingLayout scalingLayout;
    private String operation;

    @SuppressLint("ValidFragment")
    public SearchAnythingFragment (String operation){
        this.operation = operation;
    }


    public void setOperation(String operation) {
        this.operation = operation;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search_track, container, false);
        textViewSearch = view.findViewById(R.id.textViewSearch);
        final RelativeLayout searchLayout = view.findViewById(R.id.searchLayout);
        final ImageButton searchButton = view.findViewById(R.id.search_text_button);
        scalingLayout = view.findViewById(R.id.scalingLayout);
        editTextSearch = view.findViewById(R.id.editTextSearch);
        searchButton.setOnClickListener(mListener);
        editTextSearch.setOnKeyListener(keyListener);
        scalingLayout.setListener(new ScalingLayoutListener() {
            @Override
            public void onCollapsed() {
                ViewCompat.animate(textViewSearch).alpha(1).setDuration(150).start();
                ViewCompat.animate(searchLayout).alpha(0).setDuration(150).setListener(new ViewPropertyAnimatorListener() {
                    @Override
                    public void onAnimationStart(View view) {
                        textViewSearch.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(View view) {
                        searchLayout.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void onAnimationCancel(View view) {

                    }
                }).start();
            }

            @Override
            public void onExpanded() {
                ViewCompat.animate(textViewSearch).alpha(0).setDuration(200).start();
                ViewCompat.animate(searchLayout).alpha(1).setDuration(200).setListener(new ViewPropertyAnimatorListener() {
                    @Override
                    public void onAnimationStart(View view) {
                        searchLayout.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(View view) {
                        textViewSearch.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void onAnimationCancel(View view) {

                    }
                }).start();
            }

            @Override
            public void onProgress(float progress) {

            }
        });

        scalingLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (scalingLayout.getState() == State.COLLAPSED) {
                    scalingLayout.expand();
                }
            }
        });

        view.findViewById(R.id.rootLayout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (scalingLayout.getState() == State.EXPANDED) {
                    scalingLayout.collapse();

                    if(editTextSearch.getText().toString().equals(""))
                        textViewSearch.setText("Search");
                }
            }
        });
        return view;
    }
    View.OnKeyListener keyListener = new View.OnKeyListener() {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                String query = editTextSearch.getText().toString();

                if(query.equals("")){
                    textViewSearch.setText("Search");
                } else {

                    fragmentManager = getFragmentManager();
                    FragmentTransaction ft = fragmentManager.beginTransaction();
                    ft.add(R.id.fragment, ResultSearchAnythingFragment.newInstance(query,operation))
                            .addToBackStack(TAG)
                            .commit();



                    textViewSearch.setText(query);
                }
                return true;

            }
            return false;
        }
    };
    View.OnClickListener mListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.search_text_button:
                    String query = editTextSearch.getText().toString();

                    scalingLayout.collapse();

                    if(query.equals("")){
                        textViewSearch.setText("Search");
                    } else {

                        fragmentManager = getFragmentManager();
                        FragmentTransaction ft = fragmentManager.beginTransaction();
                        ft.add(R.id.fragment, ResultSearchAnythingFragment.newInstance(query,operation))
                                .addToBackStack(TAG)
                                .commit();



                        textViewSearch.setText(query);
                    }

                    break;
            }
        }
    };

}
