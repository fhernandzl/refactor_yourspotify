package com.example.yourspotify.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.yourspotify.MainActivity;
import com.example.yourspotify.R;
import com.example.yourspotify.manager.SpotifyRepository;
import com.example.yourspotify.model.FavTrack;
import com.example.yourspotify.ui.FavFragmentViewModel;
import com.example.yourspotify.ui.FavFragmentViewModelFactory;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.ArrayList;
import java.util.List;

public class TracksFavFragment extends Fragment {

    public static final String TAG = "Spotify TrackFavsFragment";
    private RecyclerView mFavTrackRecyclerView;
    private VerticalTrackAdapter trackAdapter;
    private FavFragmentViewModel tViewModel;
    private SpotifyRepository spotifyRepository;


    OnTrackFavSelectedListener callback;
    public void setOnHomeSelectedListener(OnTrackFavSelectedListener callback) {
        this.callback = callback;
    }

    public interface OnTrackFavSelectedListener {
        public void showDetailFavTrack (FavTrack t);
    }

    public void setSpotifyRepository(SpotifyRepository spotifyRepository){
        this.spotifyRepository = spotifyRepository;
    }

    public static TracksFavFragment newInstance(FragmentManager fragmentManager, String tag, String operation) {
        TracksFavFragment fragment = (TracksFavFragment) fragmentManager.findFragmentByTag(tag);
            fragment = new TracksFavFragment();

        FragmentTransaction ft = fragmentManager.beginTransaction();
        if(operation == "reboot"){
            ft.replace(R.id.fragment,fragment,tag).commit();

        } else
            ft.replace(R.id.fragment,fragment,tag).addToBackStack(null).commit();

        fragment.setSpotifyRepository(MainActivity.spotifyRepository);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        SharedPreferences preferences = this.getActivity().getSharedPreferences("settingsApp", MainActivity.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(MainActivity.FRAGMENT,TAG);
        editor.commit();
        View view = inflater.inflate(R.layout.fragment_tracksfav, container, false);

        mFavTrackRecyclerView = view.findViewById(R.id.top_tracks_RecyclerView);
        mFavTrackRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        trackAdapter = new VerticalTrackAdapter();
        mFavTrackRecyclerView.setAdapter(trackAdapter);
        //nuevos métodos
        FavFragmentViewModelFactory factory = new FavFragmentViewModelFactory(this.spotifyRepository);
        tViewModel = ViewModelProviders.of((FragmentActivity) this.getActivity(),factory).get(FavFragmentViewModel.class);
        tViewModel.getFavTracks().observeForever(new Observer<List<FavTrack>>() {
            @Override
            public void onChanged(List<FavTrack> favTracks) {
                Log.d("Observer","Changes detected, size: "+favTracks.size());
                trackAdapter.setTracks(favTracks);
            }
        });
        return view;
    }


    private class VerticalTrackHolder extends RecyclerView.ViewHolder{

        private ImageView album_image_field;
        private TextView album_name_view;

        private VerticalTrackHolder(View itemView) {
            super(itemView);

            album_image_field = (ImageView) itemView.findViewById(R.id.track_image_field);
            album_name_view = (TextView) itemView.findViewById(R.id.track_name);
        }

        private void bindTrack(final FavTrack track){

            this.album_name_view.setText(track.getName());

            Picasso.with(getContext())
                    .load(track.getImg_url())
                    .transform(new Transformation() {
                        @Override
                        public Bitmap transform(Bitmap source) {
                            final Bitmap copy = source.copy(source.getConfig(), true);
                            source.recycle();

                            return copy;
                        }

                        @Override
                        public String key() {
                            return track.getName();
                        }
                    })
                    .into(this.album_image_field);



        }

    }

    private class VerticalTrackAdapter extends RecyclerView.Adapter<VerticalTrackHolder>{

        private List<FavTrack> tracks;

        private VerticalTrackAdapter(){
            tracks = new ArrayList<FavTrack>();
        }

        public void setTracks (List<FavTrack> t) {
            this.tracks = t;
            this.notifyDataSetChanged();
        }


        @Override
        public VerticalTrackHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            LayoutInflater inflater = LayoutInflater.from(getContext());

            View view = inflater.inflate(R.layout.track_view, parent, false);

            return new VerticalTrackHolder(view);
        }


        @Override
        public void onBindViewHolder(VerticalTrackHolder holder,final int position) {
            holder.bindTrack(tracks.get(position));
            holder.album_image_field.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    callback.showDetailFavTrack(tracks.get(position));
                }
            });
        }

        @Override
        public int getItemCount() {
            return tracks.size();
        }



    }

}