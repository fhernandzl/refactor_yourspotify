package com.example.yourspotify;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.view.View;
import android.webkit.CookieManager;
import android.widget.Button;
import android.widget.NumberPicker;

import androidx.appcompat.app.AppCompatActivity;

import android.widget.TextView;
import android.widget.Toast;

import com.example.yourspotify.manager.SpotifyRepository;

public class EditProfileActivity extends AppCompatActivity {

    NumberPicker numberPicker;
    Button saveSettings;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        saveSettings = (Button) findViewById(R.id.saveSettings);

        numberPicker = (NumberPicker)findViewById(R.id.editMaxNumbertText);
        numberPicker.setMinValue(10);
        numberPicker.setMaxValue(50);
        numberPicker.computeScroll();
        cargarPreferencias();

        TextView logout = (TextView)findViewById(R.id.logout_button);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CookieManager cookieManager = CookieManager.getInstance();
                cookieManager.removeAllCookies(null);
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivityForResult(intent,1);
            }
        });
        saveSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guardarPreferencias();

                Toast.makeText(getApplicationContext(),"Configuración guardada", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void cargarPreferencias(){
        SharedPreferences preferences = getSharedPreferences("settingsApp",MODE_PRIVATE);

        Integer n = preferences.getInt("maxTracks",10);
        numberPicker.setValue(n);
    }

    private void guardarPreferencias() {

        SharedPreferences preferences = getSharedPreferences("settingsApp",MODE_PRIVATE);


        SharedPreferences.Editor editor = preferences.edit();
        Integer n = numberPicker.getValue();
        SpotifyRepository sp = MainActivity.spotifyRepository;
        sp.changeMaxObjects(n);
        editor.putInt("maxTracks",n);
        if(n>20){
            editor.putInt("maxArtists",20);
            editor.putInt("maxAlbums",20);
        }
        else{
            editor.putInt("maxArtists",n);
            editor.putInt("maxAlbums",n);
        }

        editor.commit();
    }
}
