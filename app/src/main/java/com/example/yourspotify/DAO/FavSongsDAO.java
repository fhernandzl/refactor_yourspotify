package com.example.yourspotify.DAO;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.yourspotify.model.FavTrack;

import java.util.Date;
import java.util.List;

@Dao
public interface FavSongsDAO {

    @Query("UPDATE FavSongs SET favorite=0 WHERE idSong=:idSong")
    void deleteFavTrack(String idSong);
    @Query("SELECT * FROM FavSongs WHERE favorite=1")
    LiveData<List<FavTrack>> getFavsTracks();
    @Query("SELECT * FROM FavSongs WHERE date=:currentDate")
    LiveData<List<FavTrack>> getTopTracks(Date currentDate);
    @Query("SELECT count(*) FROM FavSongs WHERE idSong=:idSong and favorite=1")
    int getAlreadyInsertedSong(String idSong);
    @Query("UPDATE FavSongs SET favorite=1 WHERE idSong=:idSong")
    void insertFavTrack(String idSong);
    @Query("DELETE FROM FavSongs WHERE favorite=0 and date <:currentDate")
    void deleteTopTracks(Date currentDate);
    @Query("SELECT count(*)FROM FavSongs WHERE date=:currentDate")
    int getCountCurrentTopTracks(Date currentDate);
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertListTracks(List<FavTrack> tracks);
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertTrack(FavTrack track);

}
