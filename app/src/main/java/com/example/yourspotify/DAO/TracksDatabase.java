package com.example.yourspotify.DAO;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.example.yourspotify.model.FavTrack;

@Database(entities = {FavTrack.class}, version = 1, exportSchema = false)
@TypeConverters(DateConverter.class)
public abstract class TracksDatabase extends RoomDatabase {

    private static TracksDatabase instance;

    public static TracksDatabase getDatabase(Context context){
        if(instance==null)
            instance = Room.databaseBuilder(context.getApplicationContext(),TracksDatabase.class,"FavSongs.db").build();
        return instance;
    }

    public abstract FavSongsDAO FavSongsDAO();

}
