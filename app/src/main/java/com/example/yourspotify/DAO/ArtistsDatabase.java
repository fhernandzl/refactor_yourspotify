package com.example.yourspotify.DAO;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.example.yourspotify.model.FavArtist;

@Database(entities = {FavArtist.class}, version = 1,exportSchema = false)
@TypeConverters(DateConverter.class)
public abstract class ArtistsDatabase extends RoomDatabase {

    private static ArtistsDatabase instance;

    public static ArtistsDatabase getDatabase(Context context){
        if(instance==null)
            instance = Room.databaseBuilder(context.getApplicationContext(),ArtistsDatabase.class,"FavArtists.db").build();
        return instance;
    }

    public abstract FavArtistsDAO FavArtistsDAO();

}
