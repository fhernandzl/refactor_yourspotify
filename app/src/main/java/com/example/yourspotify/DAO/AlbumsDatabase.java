package com.example.yourspotify.DAO;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.example.yourspotify.model.FavAlbum;

@Database(entities = {FavAlbum.class}, version = 1,exportSchema = false)
@TypeConverters(DateConverter.class)
public abstract class AlbumsDatabase extends RoomDatabase {

    private static AlbumsDatabase instance;

    public static AlbumsDatabase getDatabase(Context context){
        if(instance==null)
            instance = Room.databaseBuilder(context.getApplicationContext(),AlbumsDatabase.class,"FavAlbums.db").build();
        return instance;
    }

    public abstract FavAlbumsDAO FavAlbumsDAO();

}
