package com.example.yourspotify.DAO;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.yourspotify.model.FavArtist;

import java.util.Date;
import java.util.List;

@Dao
public interface FavArtistsDAO {

    @Query("UPDATE FavArtists SET favorite=0 WHERE idArtist=:idArtist")
    void deleteFavArtist(String idArtist);
    @Query("SELECT * FROM FavArtists WHERE favorite=1")
    LiveData<List<FavArtist>> getFavArtists();
    @Query("SELECT * FROM FavArtists WHERE date=:currentDate")
    LiveData<List<FavArtist>> getTopArtists(Date currentDate);
    @Query("SELECT count(*) FROM FavArtists WHERE idArtist=:idArtist and favorite=1")
    int getAlreadyInsertedArtist(String idArtist);
    @Query("UPDATE FavArtists SET favorite=1 WHERE idArtist=:idArtist")
    void insertFavArtist(String idArtist);
    @Query("DELETE FROM FavArtists WHERE favorite=0 and date <:currentDate")
    void deleteTopArtists(Date currentDate);
    @Query("SELECT count(*)FROM FavArtists WHERE date=:currentDate")
    int getCountCurrentTopArtists(Date currentDate);
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertListArtists(List<FavArtist> artists);
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertFavArtist(FavArtist favArtist);
}

