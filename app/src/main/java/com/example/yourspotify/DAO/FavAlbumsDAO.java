package com.example.yourspotify.DAO;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.yourspotify.model.FavAlbum;

import java.util.Date;
import java.util.List;

@Dao
public interface FavAlbumsDAO {

    @Query("UPDATE FavAlbums SET favorite=0 WHERE idAlbum=:idAlbum")
    void deleteFavAlbum(String idAlbum);
    //Nuevos métodos
    @Query("SELECT * FROM FavAlbums WHERE favorite=1")
    LiveData<List<FavAlbum>>getFavAlbums();
    @Query("SELECT * FROM FavAlbums WHERE date=:currentDate")
    LiveData<List<FavAlbum>>getTopAlbums(Date currentDate);
    @Query("SELECT count(*) FROM FavAlbums WHERE idAlbum=:idAlbum AND favorite=1")
    int getAlreadyInsertedAlbum(String idAlbum);
    @Query("UPDATE FavAlbums SET favorite=1 WHERE idAlbum=:idAlbum")
    void insertFavAlbum(String idAlbum);
    @Query("DELETE FROM FavAlbums WHERE favorite=0 and date <:currentDate")
    void deleteTopAlbums(Date currentDate);
    @Query("SELECT count(*)FROM FavAlbums WHERE date=:currentDate")
    int getCountCurrentTopAlbums(Date currentDate);
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertListAlbums(List<FavAlbum> albums);
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAlbum(FavAlbum album);
}

