package com.example.yourspotify;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.yourspotify.DAO.AlbumsDatabase;
import com.example.yourspotify.DAO.ArtistsDatabase;
import com.example.yourspotify.DAO.TracksDatabase;
import com.example.yourspotify.fragments.AlbumFavFragment;
import com.example.yourspotify.fragments.ArtistFavFragment;
import com.example.yourspotify.fragments.FavsFragment;
import com.example.yourspotify.fragments.HomeFragment;
import com.example.yourspotify.fragments.MainFragment;
import com.example.yourspotify.fragments.ResultSearchAnythingFragment;
import com.example.yourspotify.fragments.SearchAnythingFragment;
import com.example.yourspotify.fragments.SearchFragment;
import com.example.yourspotify.fragments.TopAlbumsFragment;
import com.example.yourspotify.fragments.TopArtistsFragment;
import com.example.yourspotify.fragments.TracksFavFragment;
import com.example.yourspotify.manager.GetApiContent;
import com.example.yourspotify.manager.SpotifyRepository;
import com.example.yourspotify.model.FavAlbum;
import com.example.yourspotify.model.FavArtist;
import com.example.yourspotify.model.FavTrack;

import java.util.Calendar;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class MainActivity extends AppCompatActivity implements TracksFavFragment.OnTrackFavSelectedListener,ResultSearchAnythingFragment.OnSearchSelectedListener, HomeFragment.OnHomeSelectedListener, TopAlbumsFragment.AlbumSelectedListener, TopArtistsFragment.ArtistSelectedListener, ArtistFavFragment.OnTrackFavSelectedListener, AlbumFavFragment.OnTrackFavSelectedListener {

    private static final String TAG = "Spotify MainActivity";
    public static final String FAVTRACK = "Fav track";
    public static final String FAVALBUM = "Fav album";
    public static final String FAVARTIST = "Fav artist";
    private static String AUTH_TOKEN;
    public static final String TOKEN = "TOKEN";
    public static final String FRAGMENT = "FRAGMENT";
    public static final String HOUR = "HOUR";
    private Integer maxObjects;

    public static Executor executor;
    public static SpotifyRepository spotifyRepository;


    public static String getToken(){
        return AUTH_TOKEN;
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        Calendar calendar = Calendar.getInstance();
        int actualHour = calendar.get(Calendar.HOUR_OF_DAY);
        SharedPreferences preferences = getSharedPreferences("settingsApp",MODE_PRIVATE);
        int hourToken = preferences.getInt(HOUR,-1);
        if (hourToken == -1){
            Intent intent = new Intent(this, SpotifyLoginActivity.class);
            startActivityForResult(intent,1);
        } else {
            if (actualHour - hourToken >= 1) {
                Intent intent = new Intent(this, SpotifyLoginActivity.class);
                startActivityForResult(intent, 1);
            }
            else{
                FragmentManager manager = getFragmentManager();
                AUTH_TOKEN = preferences.getString(TOKEN,"");
                AlbumsDatabase albumsDatabase = AlbumsDatabase.getDatabase(getBaseContext());
                ArtistsDatabase artistsDatabase = ArtistsDatabase.getDatabase(getBaseContext());
                TracksDatabase tracksDatabase = TracksDatabase.getDatabase(getBaseContext());
                preferences = getSharedPreferences("settingsApp",MODE_PRIVATE);
                maxObjects = preferences.getInt("maxTracks",10);
                this.spotifyRepository = SpotifyRepository.getInstance(albumsDatabase.FavAlbumsDAO(),artistsDatabase.FavArtistsDAO(),
                        tracksDatabase.FavSongsDAO(), GetApiContent.getInstance(), Executors.newSingleThreadExecutor(),maxObjects);
                manager.beginTransaction().replace(R.id.fragment_container, new MainFragment()).commit();

            }

        }

    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1){
            if (resultCode == RESULT_OK){
                AUTH_TOKEN = data.getStringExtra("AUTH_TOKEN");
                saveTokenOnPreferences();
                AlbumsDatabase albumsDatabase = AlbumsDatabase.getDatabase(getBaseContext());
                ArtistsDatabase artistsDatabase = ArtistsDatabase.getDatabase(getBaseContext());
                TracksDatabase tracksDatabase = TracksDatabase.getDatabase(getBaseContext());
                SharedPreferences preferences = getSharedPreferences("settingsApp",MODE_PRIVATE);
                maxObjects = preferences.getInt("maxTracks",10);
                this.spotifyRepository = SpotifyRepository.getInstance(albumsDatabase.FavAlbumsDAO(),artistsDatabase.FavArtistsDAO(),
                        tracksDatabase.FavSongsDAO(), GetApiContent.getInstance(), Executors.newSingleThreadExecutor(),maxObjects);


                FragmentManager manager = getFragmentManager();

                manager.beginTransaction().replace(R.id.fragment_container, new MainFragment()).commit();
            }
        }
    }

    private void saveTokenOnPreferences(){
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        SharedPreferences preferences = getSharedPreferences("settingsApp",MODE_PRIVATE);


        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(TOKEN,AUTH_TOKEN);
        editor.putInt(HOUR,hour);
        editor.commit();
    }


    @Override
    public void onAttachFragment(Fragment fragment){
        if (fragment instanceof HomeFragment) {
            HomeFragment homeFragment = (HomeFragment) fragment;
            homeFragment.setOnHomeSelectedListener(this);
        }
        if (fragment instanceof TopAlbumsFragment) {
            TopAlbumsFragment topAlbumsFragment = (TopAlbumsFragment) fragment;
            topAlbumsFragment.setOnAlbumSelectedListener(this);
        }

        if (fragment instanceof TopArtistsFragment) {
            TopArtistsFragment topArtistsFragment = (TopArtistsFragment) fragment;
            topArtistsFragment.setOnArtistSelectedListener(this);
        }
        if (fragment instanceof ResultSearchAnythingFragment){
            ResultSearchAnythingFragment resultSearchAnythingFragment = (ResultSearchAnythingFragment) fragment;
            resultSearchAnythingFragment.setOnSearchSelectedListener(this);
        }
        if (fragment instanceof TracksFavFragment){
            TracksFavFragment tracksFavFragment = (TracksFavFragment) fragment;
            tracksFavFragment.setOnHomeSelectedListener(this);
        }
        if (fragment instanceof ArtistFavFragment){
            ArtistFavFragment artistFavFragment = (ArtistFavFragment) fragment;
            artistFavFragment.setOnHomeSelectedListener(this);
        }
        if (fragment instanceof AlbumFavFragment){
            AlbumFavFragment albumFavFragment = (AlbumFavFragment) fragment;
            albumFavFragment.setOnHomeSelectedListener(this);
        }
    }


    @Override
    public void showDetailTopTrack(FavTrack t) {
        Intent intent = new Intent(this, TopTrackDetailActivity.class);

        Bundle args = new Bundle();
        args.putParcelable(FAVTRACK, t);

        intent.putExtras(args);
        startActivity(intent);
    }
    @Override
    public void showDetailTopAlbum(FavAlbum t) {
        Intent intent = new Intent(this, TopAlbumDetailActivity.class);

        Bundle args = new Bundle();
        args.putParcelable(FAVALBUM, t);

        intent.putExtras(args);
        startActivity(intent);
    }

    @Override
    public void showDetailTopArtist(FavArtist t) {
        Intent intent = new Intent(this, TopArtistDetailActivity.class);

        Bundle args = new Bundle();
        args.putParcelable(FAVARTIST, t);

        intent.putExtras(args);
        startActivity(intent);
    }

    @Override
    public void showDetailSearch(Object o) {
        Intent intent;
        if (o instanceof FavTrack){
            FavTrack t = (FavTrack) o;
            intent = new Intent(this, TopTrackDetailActivity.class);
            Bundle args = new Bundle();
            args.putParcelable(FAVTRACK, t);

            intent.putExtras(args);
            startActivity(intent);
        } else if (o instanceof FavAlbum){
            FavAlbum t = (FavAlbum) o;
            intent = new Intent(this, TopAlbumDetailActivity.class);
            Bundle args = new Bundle ();
            args.putParcelable(FAVALBUM,t);

            intent.putExtras(args);
            startActivity(intent);

        }else if (o instanceof FavArtist){
            FavArtist t = (FavArtist) o;
            intent = new Intent(this, TopArtistDetailActivity.class);
            Bundle args = new Bundle ();
            args.putParcelable(FAVARTIST,t);

            intent.putExtras(args);
            startActivity(intent);

        }


    }


    @Override
    public void showDetailFavTrack(FavTrack t) {
        Intent intent = new Intent(this, TopTrackDetailActivity.class);

        Bundle args = new Bundle();
        args.putParcelable(FAVTRACK, t);

        intent.putExtras(args);
        startActivity(intent);
    }

    @Override
    public void showDetailFavArtist(FavArtist t) {
        Intent intent = new Intent(this, TopArtistDetailActivity.class);

        Bundle args = new Bundle();
        args.putParcelable(FAVARTIST, t);

        intent.putExtras(args);
        startActivity(intent);
    }
    @Override
    public void showDetailFavAlbum(FavAlbum t) {
        Intent intent = new Intent(this, TopAlbumDetailActivity.class);

        Bundle args = new Bundle();
        args.putParcelable(FAVALBUM, t);

        intent.putExtras(args);
        startActivity(intent);
    }

}
