package com.example.yourspotify.manager;

import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.yourspotify.DAO.FavAlbumsDAO;
import com.example.yourspotify.DAO.FavArtistsDAO;
import com.example.yourspotify.DAO.FavSongsDAO;
import com.example.yourspotify.MainActivity;
import com.example.yourspotify.model.FavAlbum;
import com.example.yourspotify.model.FavArtist;
import com.example.yourspotify.model.FavTrack;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;

import kaaes.spotify.webapi.android.SpotifyApi;
import kaaes.spotify.webapi.android.SpotifyCallback;
import kaaes.spotify.webapi.android.SpotifyError;
import kaaes.spotify.webapi.android.SpotifyService;
import kaaes.spotify.webapi.android.models.Album;
import kaaes.spotify.webapi.android.models.AlbumSimple;
import kaaes.spotify.webapi.android.models.AlbumsPager;
import kaaes.spotify.webapi.android.models.Artist;
import kaaes.spotify.webapi.android.models.ArtistSimple;
import kaaes.spotify.webapi.android.models.ArtistsPager;
import kaaes.spotify.webapi.android.models.NewReleases;
import kaaes.spotify.webapi.android.models.Pager;
import kaaes.spotify.webapi.android.models.PlaylistTrack;
import kaaes.spotify.webapi.android.models.Track;
import kaaes.spotify.webapi.android.models.TracksPager;
import retrofit.client.Response;


public class GetApiContent extends AppCompatActivity {

    //TODO:: cambiar el maxObjects y poner el máximo valor de configuración que tengamos y añadir a la clase repository un atributo maxObjects que sea el que se cambie con las preferencias y que cuando haga la llamada a la BBDD se quede con los maxObjects objetos primeros

    private final MutableLiveData<List<FavArtist>> topArtistsLiveData;
    private final MutableLiveData<List<FavTrack>> topTracksLiveData;
    private final MutableLiveData<List<FavAlbum>> topAlbumsLiveData;
    private  MutableLiveData<List<Track>> tracks;
    private  MutableLiveData<List<AlbumSimple>> albums;
    private  MutableLiveData<List<Artist>> artists;
    private static GetApiContent getApiContent;
    private String AUTH_TOKEN;
    private SpotifyService spotifyService;
    private Date currentDate;
    private SpotifyRepository spotifyRepository;
    private boolean foundTrack;
    private boolean foundAlbum;
    private boolean foundArtist;
    private int countCurrentFavAlbum;
    private int countCurrentFavTrack;
    private int countCurrentFavArtist;


    public interface onCompleteTopTrackListener {
        void onComplete(List<FavTrack> tracks);
        void onError(Throwable error);
    }
    public interface onCompleteTopAlbumListener {
        void onComplete(List<FavAlbum> albums);
        void onError(Throwable error);
    }

    public interface onCompleteTopArtistListener {
        void onComplete(List<FavArtist> artists);
        void onError(Throwable error);
    }
    public interface onCompleteSearchTrackListener {
        void onComplete(List<Track> tracks);
        void onError(Throwable error);
    }

    public interface onCompleteSearchAlbumListener {
        void onComplete(List<AlbumSimple> albums);
        void onError(Throwable error);
    }

    public interface onCompleteTrackFav {
        void onComplete (FavTrack t);
        void onError(Throwable error);
    }

    public interface onCompleteArtistFav {
        void onComplete (FavArtist t);
        void onError(Throwable error);
    }
    public interface onCompleteAlbumFav {
        void onComplete (FavAlbum t);
        void onError(Throwable error);
    }


    public interface onCompleteSearchArtistListener {
        void onComplete(List<Artist> artists);
        void onError(Throwable error);
    }

    private void setServiceAPI(){
        SpotifyApi api = new SpotifyApi();
        api.setAccessToken(AUTH_TOKEN);

        spotifyService = api.getService();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public GetApiContent(){
        LocalDate date = LocalDate.now();
        currentDate = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
        AUTH_TOKEN = MainActivity.getToken();
        this.setServiceAPI();
        spotifyRepository = MainActivity.spotifyRepository;
        topArtistsLiveData =  new MutableLiveData<List<FavArtist>>();
        topTracksLiveData = new MutableLiveData<List<FavTrack>>();
        topAlbumsLiveData = new MutableLiveData<List<FavAlbum>>();
        tracks = new MutableLiveData<List<Track>>();
        albums = new MutableLiveData<List<AlbumSimple>>();
        artists = new MutableLiveData<List<Artist>>();
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public static GetApiContent getInstance(){
        if(getApiContent == null) {
            getApiContent = new GetApiContent();
        }
        LocalDate date = LocalDate.now();
        getApiContent.currentDate = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
        return getApiContent;
    }

    private void getMyTopTracks(final onCompleteTopTrackListener listener, FavSongsDAO favSongsDAO, Executor diskIO){
        Map<String, Object> options = new HashMap<>();
        options.put(SpotifyService.LIMIT, 50);

        spotifyService.getPlaylistTracks("spotifycharts", "37i9dQZEVXbMDoHDwVN2tF", options, new SpotifyCallback<Pager<PlaylistTrack>>() {
            @Override
            public void failure(SpotifyError spotifyError) {
                Log.d("GetApiContent", spotifyError.toString());

                if(listener != null)
                    listener.onError(spotifyError);
            }

            @Override
            public void success(Pager<PlaylistTrack> playlistTrackPager, Response response) {
                List<PlaylistTrack> tracks = playlistTrackPager.items;
                List<FavTrack> t = new ArrayList<FavTrack>();

                for(PlaylistTrack track : tracks){
                    Log.d("GetApiContent", track.track.name);
                    Log.d("GetApiContent", track.track.album.images.get(1).url);
                    diskIO.execute(new Runnable() {
                        @Override
                        public void run() {
                            foundTrack = (favSongsDAO.getAlreadyInsertedSong(track.track.id)==1);
                            t.add(new FavTrack(track.track.id,track.track.name,track.track.album.images.get(1).url
                                    ,track.track.external_urls.get("spotify"),currentDate,track.track.artists.get(0).name,foundTrack, track.track.album.name, track.track.album.external_urls.get("spotify"), track.track.artists.get(0).external_urls.get("spotify")));

                        }
                    });

                }

                if(listener != null)
                    listener.onComplete(t);
            }
        });
    }

    private void getTopAlbums(final onCompleteTopAlbumListener listener, FavAlbumsDAO favAlbumsDAO, Executor diskIO){
        Map<String, Object> options = new HashMap<>();
        options.put(SpotifyService.OFFSET, 0);
        options.put(SpotifyService.LIMIT, 20);
        options.put(SpotifyService.COUNTRY, "ES");



        spotifyService.getNewReleases(options, new SpotifyCallback<NewReleases>() {
            @Override
            public void failure(SpotifyError spotifyError) {
                Log.d("SearchPager", spotifyError.toString());

                if(listener != null)
                    listener.onError(spotifyError);
            }

            @Override
            public void success(NewReleases newReleases, Response response) {
                List<AlbumSimple> albums = newReleases.albums.items;
                List<FavAlbum> t = new ArrayList<FavAlbum>();

                for(final AlbumSimple albumSimple : albums){
                    diskIO.execute(new Runnable() {
                        @Override
                        public void run() {
                            int found = favAlbumsDAO.getAlreadyInsertedAlbum(albumSimple.id);
                            foundAlbum = (favAlbumsDAO.getAlreadyInsertedAlbum(albumSimple.id)==1);
                            t.add(new FavAlbum(albumSimple.id,albumSimple.name,albumSimple.images.get(1).url
                                    ,albumSimple.external_urls.get("spotify"),currentDate,foundAlbum));
                    }
                    });
                }

                if(listener != null)
                    listener.onComplete(t);

            }
        });
    }



    private void getTopArtists(final onCompleteTopArtistListener listener, FavArtistsDAO favArtistsDAO, Executor diskIO){
        Map<String, Object> options = new HashMap<>();
        options.put(SpotifyService.OFFSET, 0);
        options.put(SpotifyService.LIMIT, 20);

        final List<FavArtist> t = new ArrayList<FavArtist>();

        spotifyService.getNewReleases(options, new SpotifyCallback<NewReleases>() {
            @Override
            public void failure(SpotifyError spotifyError) {
                Log.d("SearchPager", spotifyError.toString());

                if(listener != null)
                    listener.onError(spotifyError);
            }

            @Override
            public void success(NewReleases newReleases, Response response) {
                List<AlbumSimple> albums = newReleases.albums.items;

                for(final AlbumSimple albumSimple : albums){
                    spotifyService.getAlbum(albumSimple.id, new SpotifyCallback<Album>() {
                        @Override
                        public void failure(SpotifyError spotifyError) {
                            Log.d("SearchPage Followup", spotifyError.toString());

                            if(listener != null)
                                listener.onError(spotifyError);
                        }

                        @Override
                        public void success(Album album, Response response) {

                            Log.d("SearchPage Followup", albumSimple.name);
                            Log.d("SearchPage Followup", albumSimple.id);
                            Log.d("SearchPage Followup", albumSimple.images.get(1).url);

                            List<ArtistSimple> list = album.artists;

                            spotifyService.getArtist(album.artists.get(0).id, new SpotifyCallback<Artist>() {
                                @Override
                                public void failure(SpotifyError spotifyError) {
                                    Log.d("SearchPager", spotifyError.toString());
                                    listener.onError(spotifyError);
                                }

                                @Override
                                public void success(Artist artist, Response response) {
                                    if(!artist.name.equals("Various Artists")) {
                                        diskIO.execute(new Runnable() {
                                            @Override
                                            public void run() {
                                                foundArtist = (favArtistsDAO.getAlreadyInsertedArtist(artist.id) == 1);
                                                t.add(new FavArtist(artist.id,artist.name,artist.images.get(0).url
                                                        ,artist.external_urls.get("spotify"),foundArtist,currentDate));
                                            }
                                        });

                                        if (listener != null)
                                            listener.onComplete(t);
                                    }
                                }
                            });
                        }
                    });
                }
            }
        });
    }

    public LiveData<List<Track>> searchTracksResult(String query){
        List <Track> mtracks;
        GetApiContent.onCompleteSearchTrackListener trackListener = new GetApiContent.onCompleteSearchTrackListener(){

            @Override
            public void onComplete(List<Track> tracks) {
                setSearchTracksResult(tracks);
            }

            @Override
            public void onError(Throwable error) {

            }
        };
        searchTracks(trackListener,query);
        return this.tracks;
    }
    private void setSearchTracksResult(List<Track> tracks){
        this.tracks.setValue(tracks);
    }
    public LiveData<List<AlbumSimple>>  searchAlbumsResult(String query){
        GetApiContent.onCompleteSearchAlbumListener albumListener = new onCompleteSearchAlbumListener() {
            @Override
            public void onComplete(List<AlbumSimple> albums) {
                setSearchAlbumsResult(albums);
            }

            @Override
            public void onError(Throwable error) {

            }
        };
        searchAlbums(albumListener,query);
        return this.albums;
    }
    private void setSearchAlbumsResult (List<AlbumSimple> albums){
        this.albums.setValue(albums);
    }
    public LiveData<List<Artist>> searchArtistResult(String query){
        GetApiContent.onCompleteSearchArtistListener artistListener = new onCompleteSearchArtistListener() {
            @Override
            public void onComplete(List<Artist> artists) {
                setSearchArtistResult(artists);
            }

            @Override
            public void onError(Throwable error) {

            }
        };
        searchArtist(artistListener,query);
        return this.artists;
    }

    private void setSearchArtistResult(List<Artist> artists){
        this.artists.setValue(artists);
    }

    private void searchTracks (final onCompleteSearchTrackListener listener,String query){
        spotifyService.searchTracks(query, new SpotifyCallback<TracksPager>() {
            @Override
            public void failure(SpotifyError spotifyError) {
                listener.onError(spotifyError);
            }

            @Override
            public void success(TracksPager tracksPager, Response response) {
                listener.onComplete(tracksPager.tracks.items);
            }
        });
    }
    private void searchAlbums (final onCompleteSearchAlbumListener listener,String query){
        spotifyService.searchAlbums(query, new SpotifyCallback<AlbumsPager>() {
            @Override
            public void failure(SpotifyError spotifyError) {

            }

            @Override
            public void success(AlbumsPager albumsPager, Response response) {
                listener.onComplete(albumsPager.albums.items);
            }
        });
    }

    private void searchArtist (final onCompleteSearchArtistListener listener,String query){
        spotifyService.searchArtists(query, new SpotifyCallback<ArtistsPager>() {
            @Override
            public void failure(SpotifyError spotifyError) {

            }

            @Override
            public void success(ArtistsPager artistsPager, Response response) {
                listener.onComplete(artistsPager.artists.items);
            }
        });
    }


    public LiveData<List<FavTrack>> getTracksRepository(FavSongsDAO favSongsDAO, Executor diskIO, Date currentDate){
        GetApiContent.onCompleteTopTrackListener trackListener = new GetApiContent.onCompleteTopTrackListener() {
            @Override
            public void onComplete(List<FavTrack> list) {
                setTopTracks(list);
            }

            @Override
            public void onError(Throwable error) {

            }

        };
        diskIO.execute(new Runnable() {
            @Override
            public void run() {
                countCurrentFavTrack = favSongsDAO.getCountCurrentTopTracks(currentDate);
            }
        });
        if(countCurrentFavTrack ==0){
            getMyTopTracks(trackListener,favSongsDAO,diskIO);
        } else{
            try {
                LiveData<List<FavTrack>> favTracks = favSongsDAO.getTopTracks(currentDate);
                setTopTracks(LiveDataTestUtils.getValue(favTracks));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return this.topTracksLiveData;

    }

    public LiveData<List<FavArtist>> getArtistsRepository (FavArtistsDAO favArtistsDAO, Executor diskIO, Date currentDate){

        GetApiContent.onCompleteTopArtistListener artistListener = new GetApiContent.onCompleteTopArtistListener() {
            @Override
            public void onComplete(List<FavArtist> artists) {
                setTopArtists(artists);
            }

            @Override
            public void onError(Throwable error) {

            }
        };
        diskIO.execute(new Runnable() {
            @Override
            public void run() {
                countCurrentFavArtist = favArtistsDAO.getCountCurrentTopArtists(currentDate);
            }
        });
        if (countCurrentFavArtist == 0){
            getTopArtists(artistListener,favArtistsDAO,diskIO);
        } else {
            diskIO.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        LiveData<List<FavArtist>> favArtists =  favArtistsDAO.getTopArtists(currentDate);
                        setTopArtists(LiveDataTestUtils.getValue(favArtists));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        return this.topArtistsLiveData;

    }

    public LiveData<List<FavAlbum>> getAlbumsRepository(FavAlbumsDAO favAlbumsDAO, Executor diskIO, Date currentDate){

        List<FavAlbum> topAlbumList;
        GetApiContent.onCompleteTopAlbumListener albumListener = new GetApiContent.onCompleteTopAlbumListener() {
            @Override
            public void onComplete(List<FavAlbum> albums) {

                setTopAlbums(albums);
            }

            @Override
            public void onError(Throwable error) {

            }
        };
        diskIO.execute(new Runnable() {
            @Override
            public void run() {
                countCurrentFavAlbum = favAlbumsDAO.getCountCurrentTopAlbums(currentDate);
            }
        });
        if(countCurrentFavAlbum==0){
            getTopAlbums(albumListener,favAlbumsDAO,diskIO);

        } else{
            diskIO.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        LiveData<List<FavAlbum>> favAlbums = favAlbumsDAO.getTopAlbums(currentDate);
                        List<FavAlbum> favAlbumList = LiveDataTestUtils.getValue(favAlbums);
                        setTopAlbums(favAlbumList);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        return this.topAlbumsLiveData;
    }

    private void setTopAlbums(List<FavAlbum>topAlbums){
        this.topAlbumsLiveData.postValue(topAlbums);
    }
    private void setTopTracks (List<FavTrack> topTracks){
        this.topTracksLiveData.postValue(topTracks);
    }
    private void setTopArtists (List<FavArtist> topArtists){
        this.topArtistsLiveData.postValue(topArtists);
    }


}
