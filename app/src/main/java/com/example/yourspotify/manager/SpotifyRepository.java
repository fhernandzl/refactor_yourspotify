package com.example.yourspotify.manager;


import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;

import com.example.yourspotify.DAO.FavAlbumsDAO;
import com.example.yourspotify.DAO.FavArtistsDAO;
import com.example.yourspotify.DAO.FavSongsDAO;
import com.example.yourspotify.model.FavAlbum;
import com.example.yourspotify.model.FavArtist;
import com.example.yourspotify.model.FavTrack;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executor;

import kaaes.spotify.webapi.android.models.AlbumSimple;
import kaaes.spotify.webapi.android.models.Artist;
import kaaes.spotify.webapi.android.models.Track;
import retrofit.android.MainThreadExecutor;

public class SpotifyRepository {

    private int maxObjects;
    private final FavAlbumsDAO mAlbumDAO;
    private final FavArtistsDAO mArtistsDAO;
    private final FavSongsDAO mtopTracksDAO;
    private GetApiContent mGetApiContent;
    private final Executor mDiskIO;
    private boolean mInitializedTrack = false;
    private boolean mInitializedAlbum = false;
    private boolean mInitializedArtist = false;
    private MutableLiveData<List<Track>> searchTracks =  new MutableLiveData<>();
    private MutableLiveData<List<Artist>> searchArtists =  new MutableLiveData<>();
    private MutableLiveData<List<AlbumSimple>> searchAlbums =  new MutableLiveData<>();
    private MutableLiveData<Boolean> foundTrackOnDataBase = new MutableLiveData<Boolean>();
    private MutableLiveData<Boolean> foundArtistOnDataBase = new MutableLiveData<Boolean>();
    private MutableLiveData<Boolean> foundAlbumOnDataBase = new MutableLiveData<Boolean>();

    private MutableLiveData<List<FavTrack>> tracks = new MutableLiveData<>();
    private MutableLiveData<List<FavArtist>> artists = new MutableLiveData<>();
    private MutableLiveData<List<FavAlbum>> albums = new MutableLiveData<>();

    private List<FavTrack> localTracks = new ArrayList<FavTrack>();
    private List<FavArtist> localArtists = new ArrayList<FavArtist>();
    private List<FavAlbum> localAlbums = new ArrayList<FavAlbum>();

    private static SpotifyRepository sInstance;
    private static final Object LOCK = new Object();
    private Date currentDate;

    private boolean foundTrack;
    private boolean foundAlbum;
    private boolean foundArtist;

    private boolean firstTracks;
    private boolean firstAlbums;
    private boolean firstArtists;

    @RequiresApi(api = Build.VERSION_CODES.O)
    private SpotifyRepository (final FavAlbumsDAO albumDAO,
                               FavArtistsDAO artistsDAO,
                               FavSongsDAO topTracksDAO,
                               GetApiContent getApiContent,
                               Executor diskIO, Integer maxObjects){
        foundAlbum = false;
        foundTrack = false;
        foundArtist = false;
        firstTracks = true;
        firstAlbums = true;
        firstArtists = true;
        this.maxObjects = maxObjects;
        this.mAlbumDAO = albumDAO;
        this.mArtistsDAO = artistsDAO;
        this.mtopTracksDAO = topTracksDAO;
        this.mGetApiContent = getApiContent;
        this.mDiskIO = diskIO;
        LocalDate date = LocalDate.now();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            this.currentDate = dateFormat.parse(date.toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        LiveData<List<FavAlbum>> albumData = mGetApiContent.getAlbumsRepository(mAlbumDAO,mDiskIO,currentDate);
        LiveData<List<FavTrack>> trackData = mGetApiContent.getTracksRepository(mtopTracksDAO,mDiskIO,currentDate);
        LiveData<List<FavArtist>> artistData = mGetApiContent.getArtistsRepository(mArtistsDAO,mDiskIO,currentDate);

        this.getFavTracks();
        this.getFavAlbums();
        this.getFavArtists();

        albumData.observeForever(new Observer<List<FavAlbum>>() {
            @Override
            public void onChanged(final List<FavAlbum> topAlbums) {
                mDiskIO.execute(new Runnable() {
                    @Override
                    public void run() {
                        deleteOldAlbumData();

                        mAlbumDAO.insertListAlbums(topAlbums);
                        if(firstAlbums=true){
                            List<FavAlbum> list = new ArrayList<>();
                            for(int i=0;i<maxObjects&&i<20;i++){
                                Log.d("Albums","Number: "+i+" Max objects: "+maxObjects);
                                if(i<20) list.add(topAlbums.get(i));
                            }
                            albums.postValue(list);
                            firstAlbums=false;
                        } else{
                            albums.postValue(topAlbums);
                        }
                        localAlbums = topAlbums;

                    }
                });
            }
        });
        trackData.observeForever(new Observer<List<FavTrack>>() {
            @Override
            public void onChanged(final List<FavTrack> topTracks) {
                mDiskIO.execute(new Runnable()  {
                    @Override
                    public void run() {
                        deteteOldTrackData();

                        mtopTracksDAO.insertListTracks(topTracks);
                        if(firstTracks=true){
                            List<FavTrack> list = new ArrayList<>();
                            for(int i=0;i<maxObjects;i++){
                                Log.d("Tracks","Number: "+i+" Max objects: "+maxObjects);
                                list.add(topTracks.get(i));
                            }
                            tracks.postValue(list);
                            firstTracks=false;
                        } else{
                            tracks.postValue(topTracks);
                        }
                        localTracks = topTracks;
                    }
                });

            }
        });

        artistData.observeForever(new Observer<List<FavArtist>>() {
            @Override
            public void onChanged(final List<FavArtist> topArtists) {
                mDiskIO.execute(new Runnable() {
                    @Override
                    public void run() {
                        deleteOldArtistData();

                        mArtistsDAO.insertListArtists(topArtists);
                        if(firstArtists=true){
                            List<FavArtist> list = new ArrayList<>();
                            for(int i=0;i<maxObjects&&i<19;i++){
                                Log.d("Artists","Number: "+i+" Max objects: "+maxObjects);
                                if(i<topArtists.size()) list.add(topArtists.get(i));
                            }
                            artists.postValue(list);
                            firstArtists=false;
                        } else{
                            artists.postValue(topArtists);
                        }
                        localArtists = topArtists;
                    }
                });
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public synchronized static SpotifyRepository getInstance(final FavAlbumsDAO albumDAO,
                                                             FavArtistsDAO artistsDAO,
                                                             FavSongsDAO topTracksDAO,
                                                             GetApiContent getApiContent,
                                                             Executor mDiskIO, Integer maxObjects){
        if (sInstance == null){
            synchronized (LOCK){
                sInstance = new SpotifyRepository(albumDAO,artistsDAO,topTracksDAO,getApiContent,mDiskIO,maxObjects);
            }
        }
        LocalDate date = LocalDate.now();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            sInstance.currentDate = dateFormat.parse(date.toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return sInstance;
    }
    private synchronized void initilizeDataTrack(){
        if(mInitializedTrack) return;
        mInitializedTrack = true;

        mDiskIO.execute(new Runnable() {
            @Override
            public void run() {
                if(isFetchNeededTracks()){
                    Log.d("Repositorio","initilizaDataTrack2 ---------");
                    mGetApiContent.getTracksRepository(mtopTracksDAO,mDiskIO,currentDate);
                }
            }
        });
    }

    public void changeMaxObjects(Integer n){
        List<FavTrack> tracksList = new ArrayList<>();
        List<FavAlbum> albumsList = new ArrayList<>();
        List<FavArtist> artistsList = new ArrayList<>();

        for (int i=0;i<n;i++){
            tracksList.add(localTracks.get(i));
            if (i < 19) {
                artistsList.add(localArtists.get(i));
                albumsList.add(localAlbums.get(i));
            } else if(i<20) albumsList.add(localAlbums.get(i));

        }

        tracks.postValue(tracksList);
        albums.postValue(albumsList);
        artists.postValue(artistsList);
    }

    private synchronized void initilizeDataAlbum(){
        if(mInitializedAlbum) return;
        mInitializedAlbum = true;

        mDiskIO.execute(new Runnable() {
            @Override
            public void run() {
                if(isFetchNeededAlbums()){
                    mGetApiContent.getAlbumsRepository(mAlbumDAO,mDiskIO,currentDate);

                }
            }
        });
    }

    private synchronized void initilizeDataArtist(){
        if(mInitializedArtist) return;
        mInitializedArtist = true;

        mDiskIO.execute(new Runnable() {
            @Override
            public void run() {
                if(isFetchNeededArtists()){
                    mGetApiContent.getArtistsRepository(mArtistsDAO,mDiskIO,currentDate);
                }
            }
        });
    }


    private void deleteOldAlbumData(){
        this.mAlbumDAO.deleteTopAlbums(currentDate);
    }

    private void deteteOldTrackData(){
        //this.mtopTracksDAO.deleteTopTracks(currentDate);
    }
    private void deleteOldArtistData(){
        this.mArtistsDAO.deleteTopArtists(currentDate);
    }
    private boolean isFetchNeededTracks(){
        boolean update = false;
        int countCurrentTracks = this.mtopTracksDAO.getCountCurrentTopTracks(currentDate);
        if(countCurrentTracks == 0)
            update=true;
        return update;
    }
    private boolean isFetchNeededAlbums(){
        boolean update = false;
        int countCurrrentAlbums = mAlbumDAO.getCountCurrentTopAlbums(currentDate);
        if(countCurrrentAlbums == 0)
            update=true;
        return update;
    }
    private boolean isFetchNeededArtists(){
        boolean update = false;
        int countCurrrentArtists = mArtistsDAO.getCountCurrentTopArtists(currentDate);
        if(countCurrrentArtists == 0)
            update=true;
        return update;
    }

    public LiveData<List<FavAlbum>> getFavAlbums(){
        return mAlbumDAO.getFavAlbums();
    }
    public LiveData<List<FavTrack>> getFavTracks() {
        return mtopTracksDAO.getFavsTracks();
    }
    public LiveData<List<FavArtist>> getFavArtists(){
        return mArtistsDAO.getFavArtists();
    }
    public void insertFavTrack(FavTrack favTrack){
        mDiskIO.execute(new Runnable() {
            @Override
            public void run() {
                favTrack.setDate(currentDate);
                mtopTracksDAO.insertTrack(favTrack);
                mtopTracksDAO.insertFavTrack(favTrack.getIdSong());
                changeLiveDataFoundTrack(true);
            }
        });
    }

    public void insertFavAlbum(FavAlbum album){
        mDiskIO.execute(new Runnable() {
            @Override
            public void run() {
                album.setDate(currentDate);
                mAlbumDAO.insertAlbum(album);
                mAlbumDAO.insertFavAlbum(album.getIdAlbum());
                changeLiveDataFoundAlbum(true);
            }
        });
    }
    public void insertFavArtist(FavArtist favArtist){
        mDiskIO.execute(new Runnable() {
            @Override
            public void run() {
                favArtist.setDate(currentDate);
                mArtistsDAO.insertFavArtist(favArtist);
                mArtistsDAO.insertFavArtist(favArtist.getIdArtist());
                changeLiveDataFoundArtist(true);
            }
        });
    }
    public void deleteFavTrack(FavTrack t){
        mDiskIO.execute(new Runnable() {
            @Override
            public void run() {
                mtopTracksDAO.deleteFavTrack(t.getIdSong());
                changeLiveDataFoundTrack(false);
            }
        });
    }
    public void deleteFavAlbum(final String idAlbum){
        mDiskIO.execute(new Runnable() {
            @Override
            public void run() {
                mAlbumDAO.deleteFavAlbum(idAlbum);
                changeLiveDataFoundAlbum(false);
            }
        });
    }
    public void deleteFavArtist(final String idArtist){
        mDiskIO.execute(new Runnable() {
            @Override
            public void run() {
                mArtistsDAO.deleteFavArtist(idArtist);
                changeLiveDataFoundArtist(false);
            }
        });
    }
    public boolean getAlreadyInsertFavTrack(final String idFavTrack){
        mDiskIO.execute(new Runnable() {
            @Override
            public void run() {
                changeFoundTrack(mtopTracksDAO.getAlreadyInsertedSong(idFavTrack)==1);
            }
        });
        return this.foundTrack;
    }
    public LiveData<Boolean> getAlreadyInsertFavTrackLiveData(final String idTrack){
        mDiskIO.execute(new Runnable() {
            @Override
            public void run() {
                changeLiveDataFoundTrack(mtopTracksDAO.getAlreadyInsertedSong(idTrack)==1);
            }
        });
        return this.foundTrackOnDataBase;
    }
    private void changeLiveDataFoundTrack(final boolean foundTrack){
        Executor mainExecutor = new MainThreadExecutor();
        mainExecutor.execute(new Runnable() {
            @Override
            public void run() {
                foundTrackOnDataBase.setValue(foundTrack);

            }
        });
    }
    private void changeFoundTrack(final boolean foundTrack){
        this.foundTrack = foundTrack;
    }
    public boolean getAlreadyInsertFavAlbum(final String idFavAlbum){
        mDiskIO.execute(new Runnable() {
            @Override
            public void run() {
                changeFoundAlbum(mAlbumDAO.getAlreadyInsertedAlbum(idFavAlbum)==1);
            }
        });
        return this.foundAlbum;
    }
    public LiveData<Boolean> getAlreadyInsertFavAlbumLiveData(final String idAlbum){
        mDiskIO.execute(new Runnable() {
            @Override
            public void run() {
                changeLiveDataFoundAlbum(mAlbumDAO.getAlreadyInsertedAlbum(idAlbum)==1);
            }
        });
        return this.foundAlbumOnDataBase;
    }

    private void changeLiveDataFoundAlbum(final boolean foundAlbum){
        Executor mainExecutor = new MainThreadExecutor();
        mainExecutor.execute(new Runnable() {
            @Override
            public void run() {
                foundAlbumOnDataBase.setValue(foundAlbum);

            }
        });
    }

    private void changeFoundAlbum(boolean foundAlbum){
        this.foundAlbum = foundAlbum;
    }

    public boolean getAlreadyInsertFavArtist(final String idFavArtist){
        mDiskIO.execute(new Runnable() {
            @Override
            public void run() {
                changeLiveDataFoundArtist(mArtistsDAO.getAlreadyInsertedArtist(idFavArtist)==1);
            }
        });
        return this.foundArtist;
    }
    public LiveData<Boolean> getAlreadyInsertFavArtistLiveData(final String idArtist){
        mDiskIO.execute(new Runnable() {
            @Override
            public void run() {
                changeLiveDataFoundArtist(mArtistsDAO.getAlreadyInsertedArtist(idArtist)==1);
            }
        });
        return this.foundArtistOnDataBase;
    }
    private void changeLiveDataFoundArtist(final boolean foundArtist){
        Executor mainExecutor = new MainThreadExecutor();
        mainExecutor.execute(new Runnable() {
            @Override
            public void run() {
                foundArtistOnDataBase.setValue(foundArtist);

            }
        });
    }


    private void changeFoundArtist(boolean foundArtist){
        this.foundArtist = foundArtist;
    }
    public MutableLiveData<List<FavTrack>> getTracks(){
        initilizeDataTrack();
        return tracks;
    }

    public MutableLiveData<List<FavAlbum>> getAlbums(){
        initilizeDataAlbum();
        return albums;
    }

    public MutableLiveData<List<FavArtist>> getArtists(){
        initilizeDataArtist();
        return artists;
    }

    public void setMaxObjects(int maxObjects) {
        this.maxObjects = maxObjects;
    }

    public LiveData<List<Track>> searchTracks(String query){
        LiveData<List<Track>> searchTrackData = mGetApiContent.searchTracksResult(query);
        searchTrackData.observeForever(new Observer<List<Track>>() {
            @Override
            public void onChanged(List<Track> tracks) {
                searchTracks.postValue(tracks);
            }
        });
        return this.searchTracks;
    }
    public LiveData<List<AlbumSimple>> searchAlbums(String query){
        LiveData<List<AlbumSimple>> searchAlbumData = mGetApiContent.searchAlbumsResult(query);
        searchAlbumData.observeForever(new Observer<List<AlbumSimple>>() {
            @Override
            public void onChanged(List<AlbumSimple> albumSimples) {
                searchAlbums.postValue(albumSimples);
            }
        });
        return this.searchAlbums;
    }
    public LiveData<List<Artist>> searchArtists(String query){
        LiveData<List<Artist>> searchArtistData = mGetApiContent.searchArtistResult(query);
        searchArtistData.observeForever(new Observer<List<Artist>>() {
            @Override
            public void onChanged(List<Artist> artists) {
                searchArtists.setValue(artists);
            }
        });
        return this.searchArtists;
    }
}
