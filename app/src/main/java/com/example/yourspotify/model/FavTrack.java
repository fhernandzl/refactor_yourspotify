package com.example.yourspotify.model;

import android.content.Intent;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.room.Entity;

import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity(tableName = "FavSongs")
public class FavTrack implements Parcelable {

    @Ignore
    public static final String ID = "ID";
    @Ignore
    public static final String TITLE = "TITLE";
    @Ignore
    public static final String IMG_URL = "IMG_URL";
    @Ignore
    public static final String EXT_URL = "EXT_URL";
    @Ignore
    public static final String DATE = "DATE";
    @Ignore
    public static final String FAV = "FAV";
    @Ignore
    public static final String ALBUM = "ALBUM";


    @PrimaryKey @NonNull
    private String idSong;
    @NonNull
    private String name;
    @NonNull
    private String img_url;
    @NonNull
    private String externalURI;
    @NonNull
    private String artist;
    @NonNull
    private Date date;
    @NonNull
    private boolean favorite;
    @NonNull
    private String album;
    @NonNull
    private String album_url;
    @NonNull
    private String artist_url;

    public FavTrack(String idSong, String name, String img_url, String externalURI, Date date,String artist,boolean favorite, String album, String album_url, String artist_url) {
        this.idSong = idSong;
        this.artist=artist;
        this.name = name;
        this.img_url = img_url;
        this.externalURI = externalURI;
        this.date = date;
        this.favorite = favorite;
        this.album = album;
        this.album_url = album_url;
        this.artist_url = artist_url;
    }

    public FavTrack(FavTrack track) {
        this.idSong = track.idSong;
        this.artist = track.artist;
        this.externalURI = track.externalURI;
        this.img_url = track.img_url;
        this.name = track.name;
        this.date = track.date;
        this.favorite = track.favorite;
        this.album = track.album;
        this.artist_url = track.artist_url;
        this.album_url = track.album_url;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    protected FavTrack(Parcel in){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        this.idSong = in.readString();
        this.artist = in.readString();
        this.name = in.readString();
        this.img_url = in.readString();
        this.externalURI = in.readString();
        this.favorite = in.readBoolean();
        try {
            this.date = dateFormat.parse(in.readString());
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        this.album = in.readString();
        this.album_url = in.readString();
        this.artist_url = in.readString();
    }
    @Ignore
    public FavTrack() {

    }

    @NonNull
    public Date getDate() {
        return date;
    }

    public void setDate(@NonNull Date date) {
        this.date = date;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(@NonNull String img_url) {
        this.img_url = img_url;
    }

    @NonNull
    public String getExternalURI() {
        return externalURI;
    }

    public void setExternalURI(@NonNull String externalURI) {
        this.externalURI = externalURI;
    }

    @NonNull
    public String getArtist() {
        return artist;
    }

    public void setArtist(@NonNull String artist) {
        this.artist = artist;
    }

    public String getIdSong(){
        return idSong;
    }

    public void setIdSong(String idSong){
        this.idSong=idSong;
    }

    @NonNull
    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    @NonNull
    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    @NonNull
    public String getAlbum_url() {
        return album_url;
    }

    public void setAlbum_url(@NonNull String album_url) {
        this.album_url = album_url;
    }

    @NonNull
    public String getArtist_url() {
        return artist_url;
    }

    public void setArtist_url(@NonNull String artist_url) {
        this.artist_url = artist_url;
    }

    @Override
    public String toString() {
        return "FavTrack{" +
                "idSong='" + idSong + '\'' +
                ", name='" + name + '\'' +
                ", img_url='" + img_url + '\'' +
                ", externalURI='" + externalURI + '\'' +
                ", artist='" + artist + '\'' +
                ", date=" + date +
                ", favorite=" + favorite +
                ", album='" + album + '\'' +
                ", album_url='" + album_url + '\'' +
                ", artist_url='" + artist_url + '\'' +
                '}';
    }

    public static void packageIntent(Intent intent, String idSong, String name, String img_url, String externalURI, Date date, boolean favorite, String album) {

        intent.putExtra(FavTrack.ID,idSong);
        intent.putExtra(FavTrack.DATE,date);
        intent.putExtra(FavTrack.EXT_URL,externalURI);
        intent.putExtra(FavTrack.FAV,favorite);
        intent.putExtra(FavTrack.TITLE,name);
        intent.putExtra(FavTrack.IMG_URL,img_url);
        intent.putExtra(FavTrack.ALBUM,album);

    }


    @Ignore
    public static final Creator<FavTrack> CREATOR = new Creator<FavTrack>() {
        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public FavTrack createFromParcel(Parcel in) {
            return new FavTrack(in);
        }

        @Override
        public FavTrack[] newArray(int size) {
            return new FavTrack[size];
        }
    };
    @Ignore
    @Override
    public int describeContents() {
        return 0;
    }

    @Ignore
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        dest.writeString(this.idSong);
        dest.writeString(this.artist);
        dest.writeString(this.name);
        dest.writeString(this.img_url);
        dest.writeString(this.externalURI);
        dest.writeBoolean(this.favorite);
        dest.writeString(date.toString());
        dest.writeString(this.album);
        dest.writeString(this.album_url);
        dest.writeString(this.artist_url);
    }
}
