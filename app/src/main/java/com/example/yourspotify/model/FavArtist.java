package com.example.yourspotify.model;

import android.content.Intent;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;


@Entity(tableName = "FavArtists")
public class FavArtist implements Parcelable {

    @Ignore
    public static final String ID = "ID";

    @Ignore
    public static final String USER = "User";
    @Ignore
    public static final String TITLE = "TITLE";
    @Ignore
    public static final String IMG_URL = "IMG_URL";
    @Ignore
    public static final String EXT_URL = "EXT_URL";
    @Ignore
    public static final String DATE = "DATE";
    @Ignore
    public static final String FAV = "FAV";
    @PrimaryKey @NonNull
    private String idArtist;


    @NonNull
    private String name;
    @NonNull
    private String img_url;
    @NonNull
    private String externalURI;
    @NonNull
    private Date date;
    @NonNull
    private boolean favorite;

    public FavArtist(String idArtist, String name, String img_url, String externalURI, boolean favorite, Date date) {
        this.idArtist = idArtist;
        this.name = name;
        this.img_url = img_url;
        this.externalURI = externalURI;
        this.date = date;
        this.favorite = favorite;
    }

    public FavArtist(FavArtist artist) {
        this.idArtist = artist.idArtist;
        this.name = artist.name;
        this.img_url = artist.img_url;
        this.externalURI = artist.externalURI;
        this.date = artist.date;
        this.favorite = artist.favorite;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    protected FavArtist(Parcel in){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        this.idArtist = in.readString();
        this.name = in.readString();
        this.img_url = in.readString();
        this.externalURI = in.readString();
        try {
            this.date = dateFormat.parse(in.readString());
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        this.favorite = in.readBoolean();
    }
    @Ignore
    public FavArtist() {

    }

    @NonNull
    public Date getDate() {
        return date;
    }

    public void setDate(@NonNull Date date) {
        this.date = date;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(@NonNull String img_url) {
        this.img_url = img_url;
    }

    @NonNull
    public String getExternalURI() {
        return externalURI;
    }

    public void setExternalURI(@NonNull String externalURI) {
        this.externalURI = externalURI;
    }

    public String getIdArtist(){
        return idArtist;
    }

    public void setIdArtist(String idArtist){
        this.idArtist=idArtist;
    }



    @Override
    public String toString() {
        return "FavArtist{" +
                "idArtist='" + idArtist + '\'' +
                ", name='" + name + '\'' +
                ", img_url='" + img_url + '\'' +
                ", externalURI='" + externalURI + '\'' +
                ", date=" + date +
                ", favorite=" + favorite +
                '}';
    }

    public static void packageIntent(Intent intent, String idArtist,String name,String img_url,String externalURI,Date date,boolean favorite) {

        intent.putExtra(FavArtist.ID,idArtist);
        intent.putExtra(FavArtist.TITLE,name);
        intent.putExtra(FavArtist.IMG_URL,img_url);
        intent.putExtra(FavArtist.EXT_URL,externalURI);
        intent.putExtra(FavArtist.DATE,date);
        intent.putExtra(FavArtist.FAV,favorite);

    }
    @Ignore
    public static final Creator<FavArtist> CREATOR = new Creator<FavArtist>() {
        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public FavArtist createFromParcel(Parcel in) {
            return new FavArtist(in);
        }

        @Override
        public FavArtist[] newArray(int size) {
            return new FavArtist[size];
        }
    };
    @Ignore
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override    public int describeContents() {
        return 0;
    }

    @Ignore
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        dest.writeString(this.idArtist);
        dest.writeString(this.name);
        dest.writeString(this.img_url);
        dest.writeString(this.externalURI);
        dest.writeBoolean(this.favorite);
        dest.writeString(date.toString());

    }
}
