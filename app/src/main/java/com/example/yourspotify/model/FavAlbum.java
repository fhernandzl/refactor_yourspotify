package com.example.yourspotify.model;

import android.content.Intent;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


@Entity(tableName = "FavAlbums")
public class FavAlbum implements Parcelable {

    @Ignore
    public static final String ID = "ID";

    @Ignore
    public static final String TITLE = "TITLE";
    @Ignore
    public static final String IMG_URL = "IMG_URL";
    @Ignore
    public static final String EXT_URL = "EXT_URL";
    @Ignore
    public static final String DATE = "DATE";
    @Ignore
    public static final String FAV = "FAV";

    @PrimaryKey @NonNull
    private String idAlbum;


    @NonNull
    private String title;
    @NonNull
    private String img_url;
    @NonNull
    private String externalURI;
    @NonNull
    private Date date;
    @NonNull
    private boolean favorite;

    public FavAlbum(String idAlbum, String title, String img_url, String externalURI, Date date, boolean favorite) {
        this.idAlbum = idAlbum;
        this.title = title;
        this.img_url = img_url;
        this.externalURI = externalURI;
        this.date = date;
        this.favorite = favorite;

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Ignore
    protected FavAlbum(Parcel in){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        this.idAlbum = in.readString();
        this.title = in.readString();
        this.img_url = in.readString();
        this.externalURI = in.readString();
        try {
            this.date = dateFormat.parse(in.readString());
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        this.favorite = in.readBoolean();
    }
    @Ignore
    public FavAlbum() {

    }

    @NonNull
    public String getTitle() {
        return title;
    }

    public void setTitle(@NonNull String title) {
        this.title = title;
    }

    @NonNull
    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(@NonNull String img_url) {
        this.img_url = img_url;
    }

    @NonNull
    public String getExternalURI() {
        return externalURI;
    }

    public void setExternalURI(@NonNull String externalURI) {
        this.externalURI = externalURI;
    }

    public String getIdAlbum(){
        return idAlbum;
    }

    public void setIdAlbum(String idAlbum){
        this.idAlbum=idAlbum;
    }

    @NonNull
    public boolean isFavorite() {
        return favorite;
    }


    public void setFavorite(@NonNull boolean Favorite) {
        this.favorite = favorite;
    }

    @Override
    public String toString() {
        return "FavAlbum{" +
                "idAlbum='" + idAlbum + '\'' +
                ", title='" + title + '\'' +
                ", img_url='" + img_url + '\'' +
                ", externalURI='" + externalURI + '\'' +
                ", date=" + date +
                ", favorite= ' " + favorite +'\''+
                '}';
    }

    public static void packageIntent(Intent intent, String idAlbum, String title, String img_url, String externalURI, Date date,boolean favorite) {

        intent.putExtra(FavAlbum.ID,idAlbum);
        intent.putExtra(FavAlbum.TITLE,title);
        intent.putExtra(FavAlbum.IMG_URL,img_url);
        intent.putExtra(FavAlbum.EXT_URL,externalURI);
        intent.putExtra(FavAlbum.DATE,date);
        intent.putExtra(FavAlbum.FAV,favorite);

    }
    @Ignore
    public static final Creator<FavAlbum> CREATOR = new Creator<FavAlbum>() {
        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public FavAlbum createFromParcel(Parcel in) {
            return new FavAlbum(in);
        }

        @Override
        public FavAlbum[] newArray(int size) {
            return new FavAlbum[size];
        }
    };
    @Ignore
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override    public int describeContents() {
        return 0;
    }

    @Ignore
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.idAlbum);
        dest.writeString(this.title);
        dest.writeString(this.img_url);
        dest.writeString(this.externalURI);
        dest.writeBoolean(this.favorite);
        dest.writeString(date.toString());
    }

    @NonNull
    public Date getDate() {
        return date;
    }

    public void setDate(Date currentDate) {
        this.date = currentDate;
    }
}
