package com.example.yourspotify;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.yourspotify.manager.SpotifyRepository;
import com.example.yourspotify.model.FavArtist;
import com.example.yourspotify.ui.DetailActivityViewModel;
import com.example.yourspotify.ui.DetailActivityViewModelFactory;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

public class TopArtistDetailActivity extends AppCompatActivity {

    FavArtist t;
    SpotifyRepository spotifyRepository;
    private boolean foundInDataBase;
    private DetailActivityViewModel tViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        spotifyRepository = MainActivity.spotifyRepository;
        t = getIntent().getParcelableExtra(MainActivity.FAVARTIST);
        setContentView(R.layout.activity_top_track_detail);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        final ImageView trackImage = findViewById(R.id.detail_album_image_field);
        final TextView titleView = findViewById(R.id.detail_track_title_field);
        final TextView favoriteView = findViewById(R.id.addtofav);
        final ImageView favoriteViewImage = findViewById(R.id.addtofavImage);
        if (spotifyRepository.getAlreadyInsertFavArtist(t.getIdArtist())) {
            favoriteView.setText("Delete favorits");
            favoriteViewImage.setImageResource(R.drawable.ic_iconmonstr_minus_2);
        }
        else {
            favoriteView.setText("Add favorits");
            favoriteViewImage.setImageResource(R.drawable.ic_add_black_24dp);
        }

        foundInDataBase = (spotifyRepository.getAlreadyInsertFavArtist(t.getIdArtist()));
        DetailActivityViewModelFactory factory = new DetailActivityViewModelFactory(spotifyRepository,t.getIdArtist());
        tViewModel = ViewModelProviders.of(this,factory).get(DetailActivityViewModel.class);
        tViewModel.getFoundArtistLiveData().observeForever(new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                foundInDataBase = aBoolean;
                //si está ya insertado, permitimos borrar
                if (aBoolean) {
                    favoriteView.setText("Delete favorits");
                    favoriteViewImage.setImageResource(R.drawable.ic_iconmonstr_minus_2);
                }
                else {
                    favoriteView.setText("Add favorits");
                    favoriteViewImage.setImageResource(R.drawable.ic_add_black_24dp);
                }
            }
        });
        favoriteView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                //si está en la bbdd borramos
                if (foundInDataBase)
                    spotifyRepository.deleteFavArtist(t.getIdArtist());
                else
                    spotifyRepository.insertFavArtist(t);
            }
        });



        final TextView cancelView =findViewById(R.id.detail_cancel_button);
        cancelView.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                TopArtistDetailActivity.this.finish();
            }
        });

        final LinearLayout spotifyLayout=findViewById(R.id.detail_listen_spotify_layout);
        spotifyLayout.setOnClickListener(new LinearLayout.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(t.getExternalURI()));
                startActivity(i);
            }
        });

        final Button settings = findViewById(R.id.settings);
        settings.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),EditProfileActivity.class);
                startActivity(intent);
            }
        });



        Picasso.with(this)
                .load(t.getImg_url())
                .transform(new Transformation() {
                    @Override
                    public Bitmap transform(Bitmap source) {
                        // really ugly darkening trick
                        final Bitmap copy = source.copy(source.getConfig(), true);
                        source.recycle();
                        return copy;
                    }

                    @Override
                    public String key() {
                        return "darken";
                    }
                })
                .into(trackImage);
        titleView.setText(t.getName());

        TextView albumView = findViewById(R.id.detail_track_album);
        albumView.setText("");
        TextView artistView = findViewById(R.id.detail_track_artist);
        artistView.setText("");
    }
}
