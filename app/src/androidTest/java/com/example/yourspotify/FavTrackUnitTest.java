package com.example.yourspotify;

import com.example.yourspotify.model.FavArtist;
import com.example.yourspotify.model.FavTrack;

import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import static org.junit.Assert.assertEquals;

public class FavTrackUnitTest {

    private FavTrack f;

    @Before
    public void setters(){
        f = new FavTrack();
        f.setIdSong("709CndJJB3GTUhQD0LLFse");
        f.setAlbum("Mama no");
        f.setAlbum_url("https://open.spotify.com/album/55vt962wziM1oVJ1H4lTNc?si=51pD32zuRIuXfB6kWhxDJA");
        f.setArtist_url("https://open.spotify.com/artist/4uyqaioPEdClDxU6zvYlAZ?si=siBRZhnWQLa7RqVrfdJKrw");
        f.setArtist("Pablo Lopez");
        f.setName("Mama no");
        LocalDate date = LocalDate.now();
        Date currentDate = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
        f.setDate(currentDate);
        f.setImg_url("https://www.google.com/search?q=torero+chayanne&rlz=1C1CHBF_esES877ES877&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjyjau_67XmAhWNzIUKHe4PDfAQ_AUoAnoECA0QBA&biw=1536&bih=722#imgrc=59d9w9suR2UHXM:");
        f.setFavorite(false);
        f.setExternalURI("https://open.spotify.com/album/55vt962wziM1oVJ1H4lTNc?si=51pD32zuRIuXfB6kWhxDJA");
    }

    @Test
    public void getters(){
        assertEquals("709CndJJB3GTUhQD0LLFse", f.getIdSong());
        assertEquals("Mama no", f.getAlbum());
        assertEquals("https://open.spotify.com/album/55vt962wziM1oVJ1H4lTNc?si=51pD32zuRIuXfB6kWhxDJA", f.getAlbum_url());
        assertEquals("https://open.spotify.com/artist/4uyqaioPEdClDxU6zvYlAZ?si=siBRZhnWQLa7RqVrfdJKrw", f.getArtist_url());
        assertEquals(false, f.isFavorite());
        LocalDate date = LocalDate.now();
        Date currentDate = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
        assertEquals(currentDate, f.getDate());
        assertEquals("Mama no", f.getName());
        assertEquals("https://www.google.com/search?q=torero+chayanne&rlz=1C1CHBF_esES877ES877&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjyjau_67XmAhWNzIUKHe4PDfAQ_AUoAnoECA0QBA&biw=1536&bih=722#imgrc=59d9w9suR2UHXM:", f.getImg_url());
        assertEquals("https://open.spotify.com/album/55vt962wziM1oVJ1H4lTNc?si=51pD32zuRIuXfB6kWhxDJA", f.getExternalURI());
        assertEquals("Pablo Lopez", f.getArtist());
    }

}
