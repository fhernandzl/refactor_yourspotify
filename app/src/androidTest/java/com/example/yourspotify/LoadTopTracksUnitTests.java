package com.example.yourspotify;

import android.content.Context;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.LiveData;
import androidx.room.Room;
import androidx.test.runner.AndroidJUnit4;

import com.example.yourspotify.DAO.AlbumsDatabase;
import com.example.yourspotify.DAO.ArtistsDatabase;
import com.example.yourspotify.DAO.FavAlbumsDAO;
import com.example.yourspotify.DAO.FavArtistsDAO;
import com.example.yourspotify.DAO.FavSongsDAO;
import com.example.yourspotify.DAO.TracksDatabase;
import com.example.yourspotify.model.FavArtist;
import com.example.yourspotify.model.FavTrack;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class LoadTopTracksUnitTests {

    private TracksDatabase tracksDatabase;
    private FavSongsDAO favSongsDAO;

    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

    @Before
    public void createDb(){
        Context context = getInstrumentation().getTargetContext();
        tracksDatabase = Room.inMemoryDatabaseBuilder(context,TracksDatabase.class).allowMainThreadQueries().build();

        favSongsDAO = tracksDatabase.FavSongsDAO();
    }

    @Test
    public void testInsertAllTracks() throws InterruptedException {
        List<FavTrack> favTracks = new ArrayList<FavTrack>();
        LocalDate date = LocalDate.now();
        Date currentDate = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
        FavTrack f1 = createFavTrack("709CndJJB3GTUhQD0LLFse",
                "Mama no","https://open.spotify.com/album/55vt962wziM1oVJ1H4lTNc?si=51pD32zuRIuXfB6kWhxDJA",
                "https://open.spotify.com/artist/4uyqaioPEdClDxU6zvYlAZ?si=siBRZhnWQLa7RqVrfdJKrw","Pablo Lopez","Mama no"
                ,currentDate,"https://www.google.com/search?q=torero+chayanne&rlz=1C1CHBF_esES877ES877&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjyjau_67XmAhWNzIUKHe4PDfAQ_AUoAnoECA0QBA&biw=1536&bih=722#imgrc=59d9w9suR2UHXM:",
                false, "https://open.spotify.com/album/55vt962wziM1oVJ1H4lTNc?si=51pD32zuRIuXfB6kWhxDJA");
        FavTrack f2 = createFavTrack("21jGcNKet2qwijlDFuPiPb",
                "Hollywood's Bleeding","https://open.spotify.com/album/55vt962wziM1oVJ1H4lTNc?si=51pD32zuRIuXfB6kWhxDJA",
                "https://open.spotify.com/artist/4uyqaioPEdClDxU6zvYlAZ?si=siBRZhnWQLa7RqVrfdJKrw","Benito","Dale don dale"
                ,currentDate,"https://www.google.com/search?q=torero+chayanne&rlz=1C1CHBF_esES877ES877&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjyjau_67XmAhWNzIUKHe4PDfAQ_AUoAnoECA0QBA&biw=1536&bih=722#imgrc=59d9w9suR2UHXM:",
                false, "https://open.spotify.com/album/55vt962wziM1oVJ1H4lTNc?si=51pD32zuRIuXfB6kWhxDJA");
        FavTrack f3 = createFavTrack("21jGcNKet2qwijlDkoPiPb",
                "Salio el sol","https://open.spotify.com/album/55vt962wziM1oVJ1H4lTNc?si=51pD32zuRIuXfB6kWhxDJA",
                "https://open.spotify.com/artist/4uyqaioPEdClDxU6zvYlAZ?si=siBRZhnWQLa7RqVrfdJKrw","Don omar","Diva virtual"
                ,currentDate,"https://www.google.com/search?q=torero+chayanne&rlz=1C1CHBF_esES877ES877&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjyjau_67XmAhWNzIUKHe4PDfAQ_AUoAnoECA0QBA&biw=1536&bih=722#imgrc=59d9w9suR2UHXM:",
                false, "https://open.spotify.com/album/55vt962wziM1oVJ1H4lTNc?si=51pD32zuRIuXfB6kWhxDJA");
        FavTrack f4 = createFavTrack("21jGcNKet2qwijlDkoPiOP",
                "Salio el sol","https://open.spotify.com/album/55vt962wziM1oVJ1H4lTNc?si=51pD32zuRIuXfB6kWhxDJA",
                "https://open.spotify.com/artist/4uyqaioPEdClDxU6zvYlAZ?si=siBRZhnWQLa7RqVrfdJKrw","Don omar","Torero"
                ,currentDate,"https://www.google.com/search?q=torero+chayanne&rlz=1C1CHBF_esES877ES877&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjyjau_67XmAhWNzIUKHe4PDfAQ_AUoAnoECA0QBA&biw=1536&bih=722#imgrc=59d9w9suR2UHXM:",
                false, "https://open.spotify.com/album/55vt962wziM1oVJ1H4lTNc?si=51pD32zuRIuXfB6kWhxDJA");


        favTracks.add(f1);
        favTracks.add(f2);
        favTracks.add(f3);
        favTracks.add(f4);

        favSongsDAO.insertListTracks(favTracks);
        LiveData<List<FavTrack>> favTracksObtains = favSongsDAO.getTopTracks(currentDate);
        List<FavTrack> listFavTracksObtains = LiveDataTestUtils.getValue(favTracksObtains);
        assertEquals(favTracks.get(0).getIdSong(),listFavTracksObtains.get(0).getIdSong());
        assertEquals(favTracks.get(1).getIdSong(),listFavTracksObtains.get(1).getIdSong());
        assertEquals(favTracks.get(2).getIdSong(),listFavTracksObtains.get(2).getIdSong());
        assertEquals(favTracks.get(3).getIdSong(),listFavTracksObtains.get(3).getIdSong());

    }

    @Test
    public void testGetCountAllTopTracks() throws InterruptedException {
        List<FavTrack> favTracks = new ArrayList<FavTrack>();
        LocalDate date = LocalDate.now();
        Date currentDate = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
        FavTrack f1 = createFavTrack("709CndJJB3GTUhQD0LLFse",
                "Mama no","https://open.spotify.com/album/55vt962wziM1oVJ1H4lTNc?si=51pD32zuRIuXfB6kWhxDJA",
                "https://open.spotify.com/artist/4uyqaioPEdClDxU6zvYlAZ?si=siBRZhnWQLa7RqVrfdJKrw","Pablo Lopez","Mama no"
                ,currentDate,"https://www.google.com/search?q=torero+chayanne&rlz=1C1CHBF_esES877ES877&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjyjau_67XmAhWNzIUKHe4PDfAQ_AUoAnoECA0QBA&biw=1536&bih=722#imgrc=59d9w9suR2UHXM:",
                false, "https://open.spotify.com/album/55vt962wziM1oVJ1H4lTNc?si=51pD32zuRIuXfB6kWhxDJA");
        FavTrack f2 = createFavTrack("21jGcNKet2qwijlDFuPiPb",
                "Hollywood's Bleeding","https://open.spotify.com/album/55vt962wziM1oVJ1H4lTNc?si=51pD32zuRIuXfB6kWhxDJA",
                "https://open.spotify.com/artist/4uyqaioPEdClDxU6zvYlAZ?si=siBRZhnWQLa7RqVrfdJKrw","Benito","Dale don dale"
                ,currentDate,"https://www.google.com/search?q=torero+chayanne&rlz=1C1CHBF_esES877ES877&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjyjau_67XmAhWNzIUKHe4PDfAQ_AUoAnoECA0QBA&biw=1536&bih=722#imgrc=59d9w9suR2UHXM:",
                false, "https://open.spotify.com/album/55vt962wziM1oVJ1H4lTNc?si=51pD32zuRIuXfB6kWhxDJA");
        FavTrack f3 = createFavTrack("21jGcNKet2qwijlDkoPiPb",
                "Salio el sol","https://open.spotify.com/album/55vt962wziM1oVJ1H4lTNc?si=51pD32zuRIuXfB6kWhxDJA",
                "https://open.spotify.com/artist/4uyqaioPEdClDxU6zvYlAZ?si=siBRZhnWQLa7RqVrfdJKrw","Don omar","Diva virtual"
                ,currentDate,"https://www.google.com/search?q=torero+chayanne&rlz=1C1CHBF_esES877ES877&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjyjau_67XmAhWNzIUKHe4PDfAQ_AUoAnoECA0QBA&biw=1536&bih=722#imgrc=59d9w9suR2UHXM:",
                false, "https://open.spotify.com/album/55vt962wziM1oVJ1H4lTNc?si=51pD32zuRIuXfB6kWhxDJA");
        FavTrack f4 = createFavTrack("21jGcNKet2qwijlDkoPiOP",
                "Salio el sol","https://open.spotify.com/album/55vt962wziM1oVJ1H4lTNc?si=51pD32zuRIuXfB6kWhxDJA",
                "https://open.spotify.com/artist/4uyqaioPEdClDxU6zvYlAZ?si=siBRZhnWQLa7RqVrfdJKrw","Don omar","Torero"
                ,currentDate,"https://www.google.com/search?q=torero+chayanne&rlz=1C1CHBF_esES877ES877&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjyjau_67XmAhWNzIUKHe4PDfAQ_AUoAnoECA0QBA&biw=1536&bih=722#imgrc=59d9w9suR2UHXM:",
                false, "https://open.spotify.com/album/55vt962wziM1oVJ1H4lTNc?si=51pD32zuRIuXfB6kWhxDJA");


        favTracks.add(f1);
        favTracks.add(f2);
        favTracks.add(f3);
        favTracks.add(f4);

        favSongsDAO.insertListTracks(favTracks);
        assertEquals(favSongsDAO.getCountCurrentTopTracks(currentDate),4);


    }

    public FavTrack createFavTrack(String idSong, String album, String album_url, String artist_url, String artist, String name,
                                   Date currentDate,String img_url,boolean favorite,String externalURI){
        FavTrack f = new FavTrack();
        f.setIdSong(idSong);
        f.setAlbum(album);
        f.setAlbum_url(album_url);
        f.setArtist_url(artist_url);
        f.setArtist(artist);
        f.setName(name);
        f.setDate(currentDate);
        f.setImg_url(img_url);
        f.setFavorite(favorite);
        f.setExternalURI(externalURI);
        return f;
    }

}
