package com.example.yourspotify;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.LiveData;
import androidx.room.Room;

import android.content.Context;

import com.example.yourspotify.DAO.FavSongsDAO;
import com.example.yourspotify.DAO.TracksDatabase;
import com.example.yourspotify.model.FavTrack;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;
import androidx.test.runner.AndroidJUnit4;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class FavTracksDAOUnitTest {

    private FavSongsDAO favSongsDAO;
    private TracksDatabase tracksDatabase;

    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

    @Before
    public void createDb() {
        Context context = getInstrumentation().getTargetContext();
        tracksDatabase = Room.inMemoryDatabaseBuilder(context, TracksDatabase.class).allowMainThreadQueries().build();
        favSongsDAO = tracksDatabase.FavSongsDAO();
    }

    @After
    public void closeDb() throws IOException {
        tracksDatabase.close();
    }

    @Test
    public void insertFavTrack() throws Exception {
        //create two centers
        FavTrack t = createFavTrack();

        List<FavTrack> tracks = new ArrayList<FavTrack>();
        tracks.add(t);
        favSongsDAO.insertListTracks(tracks);

        //to insert the centers in the dao
        favSongsDAO.insertFavTrack("709CndJJB3GTUhQD0LLFse");

        LiveData<List<FavTrack>> favsTracks = favSongsDAO.getFavsTracks();
        List<FavTrack> tracks1 = LiveDataTestUtils.getValue(favsTracks);
        int cont = favSongsDAO.getAlreadyInsertedSong(t.getIdSong());
        assertEquals(1, tracks1.size());
        assertEquals(1, cont);

        favSongsDAO.deleteFavTrack("709CndJJB3GTUhQD0LLFse");
    }

    @Test
    public void getFavs() throws Exception {
        FavTrack t = createFavTrack();

        List<FavTrack> tracks = new ArrayList<FavTrack>();
        tracks.add(t);
        favSongsDAO.insertListTracks(tracks);

        //to insert the centers in the dao
        favSongsDAO.insertFavTrack("709CndJJB3GTUhQD0LLFse");

        LiveData<List<FavTrack>> favsTracks = favSongsDAO.getFavsTracks();
        List<FavTrack> tracks1 = LiveDataTestUtils.getValue(favsTracks);
        assertEquals(t.getIdSong(), tracks.get(0).getIdSong());

        favSongsDAO.deleteFavTrack("709CndJJB3GTUhQD0LLFse");
    }


    @Test
    public void deleteFavTrack() throws Exception {
        FavTrack t = createFavTrack();

        List<FavTrack> tracks = new ArrayList<FavTrack>();
        tracks.add(t);
        favSongsDAO.insertListTracks(tracks);

        //to insert the centers in the dao
        favSongsDAO.insertFavTrack("709CndJJB3GTUhQD0LLFse");

        LiveData<List<FavTrack>> favsTracks = favSongsDAO.getFavsTracks();
        List<FavTrack> tracks1 = LiveDataTestUtils.getValue(favsTracks);
        assertEquals(1, tracks1.size());

        favSongsDAO.deleteFavTrack("709CndJJB3GTUhQD0LLFse");
        favsTracks = favSongsDAO.getFavsTracks();
        tracks1 = LiveDataTestUtils.getValue(favsTracks);
        assertEquals(0, tracks1.size());
    }


    public FavTrack createFavTrack(){
        FavTrack f = new FavTrack();
        f.setIdSong("709CndJJB3GTUhQD0LLFse");
        f.setAlbum("Mama no");
        f.setAlbum_url("https://open.spotify.com/album/55vt962wziM1oVJ1H4lTNc?si=51pD32zuRIuXfB6kWhxDJA");
        f.setArtist_url("https://open.spotify.com/artist/4uyqaioPEdClDxU6zvYlAZ?si=siBRZhnWQLa7RqVrfdJKrw");
        f.setArtist("Pablo Lopez");
        f.setName("Mama no");
        LocalDate date = LocalDate.now();
        Date currentDate = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
        f.setDate(currentDate);
        f.setImg_url("https://www.google.com/search?q=torero+chayanne&rlz=1C1CHBF_esES877ES877&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjyjau_67XmAhWNzIUKHe4PDfAQ_AUoAnoECA0QBA&biw=1536&bih=722#imgrc=59d9w9suR2UHXM:");
        f.setFavorite(false);
        f.setExternalURI("https://open.spotify.com/album/55vt962wziM1oVJ1H4lTNc?si=51pD32zuRIuXfB6kWhxDJA");
        return f;
    }
}
