package com.example.yourspotify;

import android.content.Context;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.LiveData;
import androidx.room.Room;
import androidx.test.runner.AndroidJUnit4;

import com.example.yourspotify.DAO.ArtistsDatabase;
import com.example.yourspotify.DAO.FavArtistsDAO;
import com.example.yourspotify.model.FavArtist;
import com.example.yourspotify.model.FavTrack;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class FavArtistUnitTestDAO {
    private FavArtistsDAO favArtistsDAO;
    private ArtistsDatabase artistsDatabase;

    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

    @Before
    public void createDb() {
        //create a mock database, using our RoomDatabase implementation (AppDatabase.class)
        Context context = getInstrumentation().getTargetContext();
        artistsDatabase = Room.inMemoryDatabaseBuilder(context, ArtistsDatabase.class).allowMainThreadQueries().build();
        //take the dao from the created database
        favArtistsDAO = artistsDatabase.FavArtistsDAO();
    }
    @After
    public void closeDb() throws IOException {
        //closing database
        artistsDatabase.close();
    }
    @Test
    public void insertFavArtist () throws Exception {
        FavArtist a = createFavArtist();

        List<FavArtist> artists = new ArrayList<FavArtist>();
        artists.add(a);
        favArtistsDAO.insertListArtists(artists);

        favArtistsDAO.insertFavArtist("0UWZUmn7sybxMCqrw9tGa7");

        LiveData<List<FavArtist>> favsArtist = favArtistsDAO.getFavArtists();
        List<FavArtist> artists1 = LiveDataTestUtils.getValue(favsArtist);
        int cont = favArtistsDAO.getAlreadyInsertedArtist(a.getIdArtist());
        assertEquals(1,artists1.size());
        assertEquals(1,cont);

        favArtistsDAO.deleteFavArtist("0UWZUmn7sybxMCqrw9tGa7");

    }

    @Test
    public void deleteFavTrack() throws Exception {
        FavArtist a = createFavArtist();

        List<FavArtist> artists = new ArrayList<FavArtist>();
        artists.add(a);
        favArtistsDAO.insertListArtists(artists);

        favArtistsDAO.insertFavArtist("0UWZUmn7sybxMCqrw9tGa7");

        LiveData<List<FavArtist>> favArtist = favArtistsDAO.getFavArtists();
        List<FavArtist> artists1 = LiveDataTestUtils.getValue(favArtist);
        assertEquals(1,artists1.size());

        favArtistsDAO.deleteFavArtist("0UWZUmn7sybxMCqrw9tGa7");
        favArtist = favArtistsDAO.getFavArtists();
        artists1 = LiveDataTestUtils.getValue(favArtist);
        assertEquals(0,artists1.size());

    }

    @Test
    public void getFavs() throws Exception {
        FavArtist t = createFavArtist();

        List<FavArtist> artists = new ArrayList<FavArtist>();
        artists.add(t);
        favArtistsDAO.insertListArtists(artists);

        //to insert the centers in the dao
        favArtistsDAO.insertFavArtist("709CndJJB3GTUhQD0LLFse");

        LiveData<List<FavArtist>> favsartists = favArtistsDAO.getFavArtists();
        List<FavArtist> artists1 = LiveDataTestUtils.getValue(favsartists);
        assertEquals(t.getIdArtist(), artists.get(0).getIdArtist());

        favArtistsDAO.deleteFavArtist("709CndJJB3GTUhQD0LLFse");
    }



    public FavArtist createFavArtist (){
        FavArtist a = new FavArtist();
        a.setIdArtist("0UWZUmn7sybxMCqrw9tGa7");
        a.setName("Juanes");
        a.setImg_url("");
        a.setExternalURI("https://open.spotify.com/artist/0UWZUmn7sybxMCqrw9tGa7?si=woLwf0r4QwyMivUrQL3wXQ");
        a.setFavorite(false);
        LocalDate date = LocalDate.now();
        Date currentDate = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
        a.setDate(currentDate);
        return a;
    }
}
