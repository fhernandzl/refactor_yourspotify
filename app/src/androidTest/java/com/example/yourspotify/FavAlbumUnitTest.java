package com.example.yourspotify;

import com.example.yourspotify.model.FavAlbum;
import com.example.yourspotify.model.FavArtist;

import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import static org.junit.Assert.assertEquals;

public class FavAlbumUnitTest {

    private FavAlbum a;

    @Before
    public void setters(){
        a = new FavAlbum();
        a.setIdAlbum("2X45SzRfAFsxgkBxgryWfF");
        a.setTitle("Mas futuro que pasado");
        a.setImg_url("http://image.url");
        a.setExternalURI("https://open.spotify.com/album/2X45SzRfAFsxgkBxgryWfF?si=NHTFXAUsTPmn8ktUl9AKGQ");
        a.setFavorite(false);
        LocalDate date = LocalDate.now();
        Date currentDate = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
        a.setDate(currentDate);
    }

    @Test
    public void getters(){
        assertEquals("2X45SzRfAFsxgkBxgryWfF", a.getIdAlbum());
        assertEquals("Mas futuro que pasado", a.getTitle());
        assertEquals("http://image.url", a.getImg_url());
        assertEquals("https://open.spotify.com/album/2X45SzRfAFsxgkBxgryWfF?si=NHTFXAUsTPmn8ktUl9AKGQ", a.getExternalURI());
        assertEquals(false, a.isFavorite());
        LocalDate date = LocalDate.now();
        Date currentDate = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
        assertEquals(currentDate, a.getDate());
    }

}
