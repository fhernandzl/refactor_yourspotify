package com.example.yourspotify;

import android.content.Context;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.LiveData;
import androidx.room.Room;
import androidx.test.runner.AndroidJUnit4;

import com.example.yourspotify.DAO.AlbumsDatabase;
import com.example.yourspotify.DAO.FavAlbumsDAO;
import com.example.yourspotify.model.FavAlbum;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class FavAlbumUnitTestDAO {
    private FavAlbumsDAO favAlbumsDAO;
    private AlbumsDatabase AlbumsDatabase;

    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

    @Before
    public void createDb() {
        //create a mock database, using our RoomDatabase implementation (AppDatabase.class)
        Context context = getInstrumentation().getTargetContext();
        AlbumsDatabase = Room.inMemoryDatabaseBuilder(context, AlbumsDatabase.class).allowMainThreadQueries().build();
        //take the dao from the created database
        favAlbumsDAO = AlbumsDatabase.FavAlbumsDAO();
    }
    @After
    public void closeDb() throws IOException {
        //closing database
        AlbumsDatabase.close();
    }
    @Test
    public void insertFavAlbum () throws Exception {
        FavAlbum a = createFavAlbum();

        List<FavAlbum> Albums = new ArrayList<FavAlbum>();
        Albums.add(a);
        favAlbumsDAO.insertListAlbums(Albums);

        favAlbumsDAO.insertFavAlbum("2X45SzRfAFsxgkBxgryWfF");

        LiveData<List<FavAlbum>> favsAlbum = favAlbumsDAO.getFavAlbums();
        List<FavAlbum> Albums1 = LiveDataTestUtils.getValue(favsAlbum);
        int cont = favAlbumsDAO.getAlreadyInsertedAlbum(a.getIdAlbum());
        assertEquals(1,Albums1.size());
        assertEquals(1,cont);

        favAlbumsDAO.deleteFavAlbum("2X45SzRfAFsxgkBxgryWfF");

    }

    @Test
    public void deleteFavTrack() throws Exception {
        FavAlbum a = createFavAlbum();

        List<FavAlbum> Albums = new ArrayList<FavAlbum>();
        Albums.add(a);
        favAlbumsDAO.insertListAlbums(Albums);

        favAlbumsDAO.insertFavAlbum("2X45SzRfAFsxgkBxgryWfF");

        LiveData<List<FavAlbum>> favAlbum = favAlbumsDAO.getFavAlbums();
        List<FavAlbum> Albums1 = LiveDataTestUtils.getValue(favAlbum);
        assertEquals(1,Albums1.size());

        favAlbumsDAO.deleteFavAlbum("2X45SzRfAFsxgkBxgryWfF");
        favAlbum = favAlbumsDAO.getFavAlbums();
        Albums1 = LiveDataTestUtils.getValue(favAlbum);
        assertEquals(0,Albums1.size());

    }


    @Test
    public void getFavs() throws Exception {
        FavAlbum t = createFavAlbum();

        List<FavAlbum> Albums = new ArrayList<FavAlbum>();
        Albums.add(t);
        favAlbumsDAO.insertListAlbums(Albums);

        //to insert the centers in the dao
        favAlbumsDAO.insertFavAlbum("2X45SzRfAFsxgkBxgryWfF");

        LiveData<List<FavAlbum>> favsAlbums = favAlbumsDAO.getFavAlbums();
        List<FavAlbum> Albums1 = LiveDataTestUtils.getValue(favsAlbums);
        assertEquals(t.getIdAlbum(), Albums.get(0).getIdAlbum());

        favAlbumsDAO.deleteFavAlbum("2X45SzRfAFsxgkBxgryWfF");
    }


    public FavAlbum createFavAlbum (){
        FavAlbum a = new FavAlbum();
        a.setIdAlbum("2X45SzRfAFsxgkBxgryWfF");
        a.setTitle("Mas futuro que pasado");
        a.setImg_url("");
        a.setExternalURI("https://open.spotify.com/album/2X45SzRfAFsxgkBxgryWfF?si=NHTFXAUsTPmn8ktUl9AKGQ");
        a.setFavorite(false);
        LocalDate date = LocalDate.now();
        Date currentDate = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
        a.setDate(currentDate);
        return a;
    }
}
