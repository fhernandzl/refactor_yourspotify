package com.example.yourspotify;

import com.example.yourspotify.model.FavArtist;

import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import static org.junit.Assert.assertEquals;

public class FavArtistUnitTest {

    private FavArtist a;

    @Before
    public void setters(){
        a = new FavArtist();
        a.setIdArtist("0UWZUmn7sybxMCqrw9tGa7");
        a.setName("Juanes");
        a.setImg_url("http://image.url");
        a.setExternalURI("https://open.spotify.com/artist/0UWZUmn7sybxMCqrw9tGa7?si=woLwf0r4QwyMivUrQL3wXQ");
        a.setFavorite(false);
        LocalDate date = LocalDate.now();
        Date currentDate = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
        a.setDate(currentDate);
    }

    @Test
    public void getters(){
        assertEquals("0UWZUmn7sybxMCqrw9tGa7", a.getIdArtist());
        assertEquals("Juanes", a.getName());
        assertEquals("http://image.url", a.getImg_url());
        assertEquals("https://open.spotify.com/artist/0UWZUmn7sybxMCqrw9tGa7?si=woLwf0r4QwyMivUrQL3wXQ", a.getExternalURI());
        assertEquals(false, a.isFavorite());
        LocalDate date = LocalDate.now();
        Date currentDate = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
        assertEquals(currentDate, a.getDate());
    }

}
