package com.example.yourspotify;

import android.content.Context;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.LiveData;
import androidx.room.Room;
import androidx.test.runner.AndroidJUnit4;

import com.example.yourspotify.DAO.AlbumsDatabase;
import com.example.yourspotify.DAO.ArtistsDatabase;
import com.example.yourspotify.DAO.FavAlbumsDAO;
import com.example.yourspotify.DAO.FavArtistsDAO;
import com.example.yourspotify.DAO.AlbumsDatabase;
import com.example.yourspotify.model.FavArtist;
import com.example.yourspotify.model.FavAlbum;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class LoadTopAlbumsUnitTests {

    private AlbumsDatabase AlbumsDatabase;
    private FavAlbumsDAO favAlbumsDAO;

    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

    @Before
    public void createDb(){
        Context context = getInstrumentation().getTargetContext();
        AlbumsDatabase = Room.inMemoryDatabaseBuilder(context,AlbumsDatabase.class).allowMainThreadQueries().build();

        favAlbumsDAO = AlbumsDatabase.FavAlbumsDAO();
    }

    @Test
    public void testInsertAllAlbums() throws InterruptedException {
        List<FavAlbum> favAlbums = new ArrayList<FavAlbum>();
        LocalDate date = LocalDate.now();
        Date currentDate = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
        FavAlbum f1 = createFavAlbum("7vcXrSc3D2EoyaBpFOrIvg","Álbum Uno","https://www.google.com/search?q=torero+chayanne&rlz=1C1CHBF_esES877ES877&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjyjau_67XmAhWNzIUKHe4PDfAQ_AUoAnoECA0QBA&biw=1536&bih=722#imgrc=59d9w9suR2UHXM:",
        "https://open.spotify.com/album/7vcXrSc3D2EoyaBpFOrIvg?si=h4EpXtlJQUWuZPeEN1d4gA",currentDate,false);
        FavAlbum f2 = createFavAlbum("7n3QJc7TBOxXtlYh4Ssll8","21","https://www.google.com/search?q=torero+chayanne&rlz=1C1CHBF_esES877ES877&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjyjau_67XmAhWNzIUKHe4PDfAQ_AUoAnoECA0QBA&biw=1536&bih=722#imgrc=59d9w9suR2UHXM:",
                "https://open.spotify.com/album/7vcXrSc3D2EoyaBpFOrIvg?si=h4EpXtlJQUWuZPeEN1d4gA",currentDate,false);
        FavAlbum f3 = createFavAlbum("5rGpmYiAJvX54ELjvzt02N","Mismo sitio,distinto lugar","https://www.google.com/search?q=torero+chayanne&rlz=1C1CHBF_esES877ES877&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjyjau_67XmAhWNzIUKHe4PDfAQ_AUoAnoECA0QBA&biw=1536&bih=722#imgrc=59d9w9suR2UHXM:",
                "https://open.spotify.com/album/7vcXrSc3D2EoyaBpFOrIvg?si=h4EpXtlJQUWuZPeEN1d4gA",currentDate,false);
        FavAlbum f4 = createFavAlbum("19lX6EAXyt4uROdcsgnoiX","Ídolo","https://www.google.com/search?q=torero+chayanne&rlz=1C1CHBF_esES877ES877&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjyjau_67XmAhWNzIUKHe4PDfAQ_AUoAnoECA0QBA&biw=1536&bih=722#imgrc=59d9w9suR2UHXM:",
                "https://open.spotify.com/album/7vcXrSc3D2EoyaBpFOrIvg?si=h4EpXtlJQUWuZPeEN1d4gA",currentDate,false);
        favAlbums.add(f1);
        favAlbums.add(f2);
        favAlbums.add(f3);
        favAlbums.add(f4);

        favAlbumsDAO.insertListAlbums(favAlbums);
        LiveData<List<FavAlbum>> favAlbumsObtains = favAlbumsDAO.getTopAlbums(currentDate);
        List<FavAlbum> listFavAlbumsObtains = LiveDataTestUtils.getValue(favAlbumsObtains);
        assertEquals(favAlbums.get(0).getIdAlbum(),listFavAlbumsObtains.get(0).getIdAlbum());
        assertEquals(favAlbums.get(1).getIdAlbum(),listFavAlbumsObtains.get(1).getIdAlbum());
        assertEquals(favAlbums.get(2).getIdAlbum(),listFavAlbumsObtains.get(2).getIdAlbum());
        assertEquals(favAlbums.get(3).getIdAlbum(),listFavAlbumsObtains.get(3).getIdAlbum());

    }
    @Test
    public void testGetCountAllTopAlbum () throws InterruptedException {
        List<FavAlbum> favAlbums = new ArrayList<FavAlbum>();
        LocalDate date = LocalDate.now();
        Date currentDate = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
        FavAlbum f1 = createFavAlbum("7vcXrSc3D2EoyaBpFOrIvg","Álbum Uno","https://www.google.com/search?q=torero+chayanne&rlz=1C1CHBF_esES877ES877&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjyjau_67XmAhWNzIUKHe4PDfAQ_AUoAnoECA0QBA&biw=1536&bih=722#imgrc=59d9w9suR2UHXM:",
                "https://open.spotify.com/album/7vcXrSc3D2EoyaBpFOrIvg?si=h4EpXtlJQUWuZPeEN1d4gA",currentDate,false);
        FavAlbum f2 = createFavAlbum("7n3QJc7TBOxXtlYh4Ssll8","21","https://www.google.com/search?q=torero+chayanne&rlz=1C1CHBF_esES877ES877&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjyjau_67XmAhWNzIUKHe4PDfAQ_AUoAnoECA0QBA&biw=1536&bih=722#imgrc=59d9w9suR2UHXM:",
                "https://open.spotify.com/album/7vcXrSc3D2EoyaBpFOrIvg?si=h4EpXtlJQUWuZPeEN1d4gA",currentDate,false);
        FavAlbum f3 = createFavAlbum("5rGpmYiAJvX54ELjvzt02N","Mismo sitio,distinto lugar","https://www.google.com/search?q=torero+chayanne&rlz=1C1CHBF_esES877ES877&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjyjau_67XmAhWNzIUKHe4PDfAQ_AUoAnoECA0QBA&biw=1536&bih=722#imgrc=59d9w9suR2UHXM:",
                "https://open.spotify.com/album/7vcXrSc3D2EoyaBpFOrIvg?si=h4EpXtlJQUWuZPeEN1d4gA",currentDate,false);
        FavAlbum f4 = createFavAlbum("19lX6EAXyt4uROdcsgnoiX","Ídolo","https://www.google.com/search?q=torero+chayanne&rlz=1C1CHBF_esES877ES877&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjyjau_67XmAhWNzIUKHe4PDfAQ_AUoAnoECA0QBA&biw=1536&bih=722#imgrc=59d9w9suR2UHXM:",
                "https://open.spotify.com/album/7vcXrSc3D2EoyaBpFOrIvg?si=h4EpXtlJQUWuZPeEN1d4gA",currentDate,false);
        favAlbums.add(f1);
        favAlbums.add(f2);
        favAlbums.add(f3);
        favAlbums.add(f4);

        favAlbumsDAO.insertListAlbums(favAlbums);
        assertEquals(favAlbumsDAO.getCountCurrentTopAlbums(currentDate),4);


    }


    public FavAlbum createFavAlbum(String idAlbum, String title, String img_url, String externalURI, Date date, boolean favorite){
        FavAlbum f = new FavAlbum();
        f.setIdAlbum(idAlbum);
        f.setTitle(title);
        f.setImg_url(img_url);
        f.setExternalURI(externalURI);
        f.setDate(date);
        f.setFavorite(favorite);
        return f;
    }

}
